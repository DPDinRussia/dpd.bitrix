<?php
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\SystemException;

use \Ipolh\DPD\API\User as API;
use \Ipolh\DPD\Shipment;

if (!$USER->IsAdmin()
	|| !Loader::includeModule('ipol.dpd')
) {
	return false;
}

Loc::loadMessages(__FILE__);

$arSites = array();
$arSitesFull = array();
$arPersoneTypes = array();

$dbSites = CSite::GetList($by = "active", $order = "desc", array("ACTIVE" => "Y"));
while($site = $dbSites->Fetch())
{	
	$arSites[] = $site["ID"];
	$arSitesFull[] = $site;

	$resPersoneTypes = CSalePersonType::GetList(array("SORT" => "ASC"), array("LID" => $site['ID'], 'ACTIVE' => 'Y'));
	while($arPersoneType = $resPersoneTypes->Fetch()) {
		$arPersoneType['SITE_ID'] = $site['ID'];
		$arPersoneTypes[] = $arPersoneType;
	}
}


$rsOrderStatuses = \CSaleStatus::GetList(array('SORT' => 'ASC'), array('LID'  => LANGUAGE_ID));
$arOrderStatuses = array();

while($arOrderStatus = $rsOrderStatuses->Fetch()) {
	$arOrderStatus['NAME'] = $arOrderStatus['NAME'] .' ['. $arOrderStatus['ID'] .']';
	$arOrderStatuses[] = $arOrderStatus;
}

$rsCurrencies = CCurrency::GetList($by = '', $order = '');
$arCurrencies = array();
while ($arCurrency = $rsCurrencies->GetNext()) {
	$arCurrencies[$arCurrency['CURRENCY']] = $arCurrency['FULL_NAME'];
}

$aTabs = array(
	array(
		"DIV" => "IPOLH_DPD_OPTIONS_TAB_COMMON",
		"TAB" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON"),
		"ICON" => "support_settings",
		"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_TITLE"),
		"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_HELP"),
		"OPTIONS" => array(),
		"CONTROLS" => array(
			"API_TABS" => array(
				"TYPE"  => "TABS",
				"ITEMS" => array(
					array(
						"DIV"      => "IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU",
						"TAB"      => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU'),
						"ICON"     => "support_settings",
						"TITLE"    => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU_TITLE'),
						"HELP"     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU_HELP"),
						"OPTIONS"  => array(),
						"CONTROLS" => array(
							"KLIENT_NUMBER" => array(
								"TYPE" => "STRING",
								"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_NUMBER"),
								"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_NUMBER_HELP"),
							),

							"KLIENT_KEY" => array(
								"TYPE" => "STRING",
								"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_KEY"),
								"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_KEY_HELP"),
							),

							"KLIENT_CURRENCY" => array(
								"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY"),
								"COMMENT" => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_RU_HELP"),
								"TYPE"    => "SELECT",
								"ITEMS"   => $arCurrencies,
								"DEFAULT" => "RUB",
							),
						),
					),

					array(
						"DIV"      => "IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ",
						"TAB"      => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ'),
						"ICON"     => "support_settings",
						"TITLE"    => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ_TITLE'),
						"HELP"     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ_HELP"),
						"OPTIONS"  => array(),
						"CONTROLS" => array(
							"KLIENT_NUMBER_KZ" => array(
								"TYPE" => "STRING",
								"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_NUMBER"),
								"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_NUMBER_HELP"),
							),

							"KLIENT_KEY_KZ" => array(
								"TYPE" => "STRING",
								"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_KEY"),
								"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_KEY_HELP"),
							),

							"KLIENT_CURRENCY_KZ" => array(
								"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY"),
								"COMMENT" => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_KZ_HELP"),
								"TYPE"    => "SELECT",
								"ITEMS"   => $arCurrencies,
								"DEFAULT" => "KZT",
							),
						),
					),

					array(
						"DIV"      => "IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY",
						"TAB"      => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY'),
						"ICON"     => "support_settings",
						"TITLE"    => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY_TITLE'),
						"HELP"     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY_HELP"),
						"OPTIONS"  => array(),
						"CONTROLS" => array(
							"KLIENT_NUMBER_BY" => array(
								"TYPE" => "STRING",
								"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_NUMBER"),
								"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_NUMBER_HELP"),
							),

							"KLIENT_KEY_BY" => array(
								"TYPE" => "STRING",
								"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_KEY"),
								"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_CLIENT_KEY_HELP"),
							),

							"KLIENT_CURRENCY_BY" => array(
								"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY"),
								"COMMENT" => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_BY_HELP"),
								"TYPE"    => "SELECT",
								"ITEMS"   => $arCurrencies,
								"DEFAULT" => "BYR",
							),
						),
					),

					array(
						"DIV"      => "IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG",
						"TAB"      => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG'),
						"ICON"     => "support_settings",
						"TITLE"    => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG_TITLE'),
						"HELP"     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG_HELP"),
						"OPTIONS"  => array(),
						"CONTROLS" => array(
							"KLIENT_CURRENCY_KG" => array(
								"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY"),
								"COMMENT" => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_KG_HELP"),
								"TYPE"    => "SELECT",
								"ITEMS"   => $arCurrencies,
								"DEFAULT" => "KGS",
							),
						),
					),

					array(
						"DIV"      => "IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM",
						"TAB"      => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM'),
						"ICON"     => "support_settings",
						"TITLE"    => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM_TITLE'),
						"HELP"     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM_HELP"),
						"OPTIONS"  => array(),
						"CONTROLS" => array(
							"KLIENT_CURRENCY_KG" => array(
								"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY"),
								"COMMENT" => Loc::getMessage("IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_AM_HELP"),
								"TYPE"    => "SELECT",
								"ITEMS"   => $arCurrencies,
								"DEFAULT" => "AMD",
							),
						),
					),
				),
			),

			"API_DEF_COUNTRY" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_API_DEF_COUNTRY"),
				"HELP"  => Loc::getMessage("IPOLH_DPD_OPTIONS_API_DEF_COUNTRY_HELP"),
				"TYPE"  => "SELECT",
				"ITEMS" => array(
					""   => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU'),
					"KZ" => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ'),
					"BY" => Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY'),
				),
			),

			"IS_TEST" => array(
				"TYPE" => "CHECKBOX",
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_IS_TEST"),
				"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_IS_TEST_HELP"),
			),

			"SHOW_ADMIN_BUTTON" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_SHOW_ADMIN_BUTTON"),
				"HELP"  => '',
				"TYPE"  => "SELECT",
				"ITEMS" => array(
					""       => Loc::getMessage('IPOLH_DPD_OPTIONS_SHOW_ADMIN_BUTTON_ONLY_DPD'),
					"ALWAYS" => Loc::getMessage('IPOLH_DPD_OPTIONS_SHOW_ADMIN_BUTTON_ALWAYS'),
				),
				"DEFAULT" => "",
			),

            "REQUIRED_IS_SELECT_PVZ" => array(
                "TYPE" => "CHECKBOX",
                "TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_REQUIRED_IS_SELECT_PVZ"),
            ),

			"TYPE_DISPLAYED_PVZ" => array(
                "TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_TYPE_DISPLAYED_PVZ"),
                "TYPE" => "SELECT",
                "ITEMS" => array(
					'' => '',
                    Loc::getMessage('IPOLH_DPD_OPTIONS_TERMINAL_TYPE_POSTOMAT_KEY') => Loc::getMessage('IPOLH_DPD_OPTIONS_TERMINAL_TYPE_POSTOMAT_NAME'),
                    Loc::getMessage('IPOLH_DPD_OPTIONS_TERMINAL_TYPE_PVP_KEY') => Loc::getMessage('IPOLH_DPD_OPTIONS_TERMINAL_TYPE_PVP_NAME'),
                ),
                "MULTIPLE" => true,
            ),

            "ORDER_ID" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_ORDER_ID"),
            	"TYPE"  => "SELECT",
				"ITEMS" => [
					"ID"             => Loc::getMessage("IPOLH_DPD_OPTIONS_ORDER_ID_ID"),
					"ACCOUNT_NUMBER" => Loc::getMessage("IPOLH_DPD_OPTIONS_ORDER_ID_NUMBER"),
				],
				"HELP"  => Loc::getMessage("IPOLH_DPD_OPTIONS_ORDER_ID_HELP"),
				"DEFAULT" => "ID",
            ),

            'NOT_INCLUDE_MAP_API' => array(
            	"TYPE" => "CHECKBOX",
            	"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_NOT_INCLUDE_MAP_API"),
            	"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_NOT_INCLUDE_MAP_API_HELP"),
			),
			
			'DEBUG_HEADER' => array(
				"TYPE"  => "HEADER",
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_DEBUG_HEADER"),
			),

			"LOG_LEVEL" => array(
				"TYPE"  => "SELECT",
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL"),
				"ITEMS" => [
					\Ipolh\DPD\Debug\Log::LEVEL_NONE   => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_NONE"),
					// \Ipolh\DPD\Log::LEVEL_DEBUG   => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_DEBUG"),
					// \Ipolh\DPD\Log::LEVEL_NOTICE  => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_NOTICE"),
					// \Ipolh\DPD\Log::LEVEL_WARNING => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_WARNING"),
					// \Ipolh\DPD\Log::LEVEL_ERROR   => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_ERROR"),
					\Ipolh\DPD\Debug\Log::LEVEL_ALL    => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_ALL"),
				],
				// "MULTIPLE" => true,
				"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_LEVEL_HELP")
			),

			"LOG_FILE_PATH" => array(
				"TYPE"    => "STRING",
				"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_LOG_FILE_PATH"),
				"DEFAULT" => "bitrix/modules/ipol.dpd/data/debug.log",
			),
		),
	),

	array(
		"DIV" => "IPOLH_DPD_OPTIONS_TAB_LOCATION",
		"TAB" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_LOCATION"),
		"ICON" => "support_settings",
		"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_LOCATION_TITLE"),
		"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_LOCATION_HELP"),
		"OPTIONS" => array(),
		"CONTROLS" => array(
			"LOCATION_COUNTRY" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOCATION_COUNTRY"),
				"TYPE" => "SELECT",
				"ITEMS" => $arLocationTypes = array_flip(\CSaleLocation::getTypes()),
				"DEFAULT" => "1",
			),
			
			"LOCATION_REGION" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOCATION_REGION"),
				"TYPE" => "SELECT",
				"ITEMS" => $arLocationTypes,
				"DEFAULT" => "3",
			),

			"LOCATION_AREA" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOCATION_AREA"),
				"TYPE" => "SELECT",
				"ITEMS" => $arLocationTypes,
				"DEFAULT" => "4",
			),

			"LOCATION_CITY" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOCATION_CITY"),
				"TYPE" => "SELECT",
				"ITEMS" => $arLocationTypes,
				"DEFAULT" => "5",
			),

			"LOCATION_VILLAGE" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_LOCATION_VILLAGE"),
				"TYPE" => "SELECT",
				"ITEMS" => $arLocationTypes,
				"DEFAULT" => "6",
			),
		),
	),

	array(
		'DIV'      => 'IPOLH_DPD_OPTIONS_TAB_DIMENSIONS',
		'TAB'      => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_DIMENSIONS"),
		'ICON'     => '',
		'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_DIMENSIONS_TITLE"),
		'HELP'     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_DIMENSIONS_HELP"),
		'OPTIONS'  => array(),
		'CONTROLS' => array(
			'USE_MODE' => array(
				'TYPE' => 'SELECT',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_USE_MODE"),
				'DEFAULT' => 'ORDER',
				'ITEMS' => [
					'ORDER' => Loc::getMessage("IPOLH_DPD_OPTIONS_USE_MODE_ORDER"),
					'ITEM'  => Loc::getMessage("IPOLH_DPD_OPTIONS_USE_MODE_ITEM"),
				]
			),

			'WEIGHT' => array(
				'TYPE' => 'TEXT',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_WEIGHT"),
				'DEFAULT' =>  '1000',
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_WEIGHT_REQUIRED"),
				),
			),

			'LENGTH' => array(
				'TYPE' => 'TEXT',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_LENGTH"),
				'DEFAULT' =>  '200',
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_LENGTH_REQUIRED"),
				),
			),

			'WIDTH' => array(
				'TYPE' => 'TEXT',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_WIDTH"),
				'DEFAULT' =>  '100',
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_WIDTH_REQUIRED"),
				),
			),

			'HEIGHT' => array(
				'TYPE' => 'TEXT',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_HEIGHT"),
				'DEFAULT' =>  '200',
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_HEIGHT_REQUIRED"),
				),
			),
		),
	),

	array(
		"DIV" => "IPOLH_DPD_OPTIONS_TAB_CALCULATE",
		"TAB" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_CALCULATE"),
		"ICON" => "support_settings",
		"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_CALCULATE_TITLE"),
		"OPTIONS" => array(),
		"CONTROLS" => array(
			"TARIFF_OFF" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_EXCLUDE_TARIFF"),
				"TYPE" => "SELECT",
				"ITEMS" => \Ipolh\DPD\Calculator::TariffList(),
				"MULTIPLE" => true,
				"NULL" => "",
			),

			"DEFAULT_TARIFF_CODE" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_DEFAULT_TARIFF_CODE"),
				"TYPE" => "SELECT",
				"ITEMS" => \Ipolh\DPD\Calculator::TariffList(),
				"NULL" => "",
				"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_DEFAULT_TARIFF_CODE_HELP"),
				"DEFAULT" => "PCL",
			),

			"DEFAULT_TARIFF_THRESHOLD" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_DEFAULT_TARIFF_THRESHOLD"),
				"TYPE" => "STRING",
				"DEFAULT" => "500",
			),

			"DECLARED_VALUE" => array(
				"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_DECLARED_VALUE"),
				"TYPE"    => "CHECKBOX",
				"DEFAULT" => 1,
				"HELP"    => Loc::getMessage("IPOLH_DPD_OPTIONS_DECLARED_VALUE_HELP"),
			),

			"CALCULATE_BY_PARCEL" => array(
				"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_CALCULATE_BY_PARCEL"),
				"TYPE"    => "CHECKBOX",
				"DEFAULT" => 0,
				"HELP"    => Loc::getMessage("IPOLH_DPD_OPTIONS_CALCULATE_BY_PARCEL_HELP"),
			),

			"ROUND_TO" => array(
				"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_CALCULATE_ROUNDING"),
				"TYPE"    => "TEXT",
				"DEFAULT" => 0,
				"HELP"    => Loc::getMessage("IPOLH_DPD_OPTIONS_CALCULATE_ROUNDING_HELP")
 			),

 			"ADD_DELIVERY_DAY" => array(
 				"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_ADD_DELIVERY_DAY"),
 				"TYPE"    => "TEXT",
 				"DEFAULT" => 0
			),
			 
			"DEFAULT_PRICE" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_DEFAULT_PRICE"),
				"HELP"  => Loc::getMessage("IPOLH_DPD_OPTIONS_DEFAULT_PRICE_HELP"),
				"TYPE"  => "TEXT",
			),

			"COMMISSIONS_HEADER" => array(
				"TITLE"   => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_HEADER"),
				"TYPE"    => "HEADER",
			),

			'COMMISSION_HELP' => array(
				'TYPE' => 'COMMENT',
				'HELP' => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_HELP"),
			),

			"COMMISSIONS_TABS" => array(
				"TYPE"  => "TABS",
				"ITEMS" => function($formValues) use ($arPersoneTypes) {
					$ret = array();

					foreach ($arPersoneTypes as $arPersoneType) {
						$rsPayments = \CSalePaySystem::GetList(
							$arOrder = array('SORT' => 'ASC', 'NAME' => 'ASC'),
							$arFilter = array('ACTIVE' => 'Y', 'PERSON_TYPE_ID' => $arPersoneType['ID'])
						);

						$arPayments = array();
						while ($arPayment = $rsPayments->Fetch()) {
							$arPayments[$arPayment['ID']] = $arPayment['NAME'];
						}

						$disabled = !($formValues && $formValues['COMMISSION_NPP_CHECK_'. $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID']]);

						$ret[] = array(
							'DIV'      => sprintf('IPOLH_DPD_OPTIONS_CALCULATE_SUBTAB_COMMISSIONS_%s_%s', $arPersoneType['ID'], $arPersoneType['SITE_ID']),
							'TAB'      => sprintf('(%s) %s (%s)', $arPersoneType['ID'], $arPersoneType['NAME'], $arPersoneType['SITE_ID']),
							'CONTROLS' => array(
								'COMMISSION_NPP_CHECK_'. $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] => array(
									'TYPE'  => 'CHECKBOX',
									'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_NPP_CHECK"),
									'ATTRS' => array(
										'onchange' => ""
											."BX('IPOLH_DPD_OPTIONS_COMMISSION_NPP_PERCENT_". $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] ."').disabled = !this.checked;"
											."BX('IPOLH_DPD_OPTIONS_COMMISSION_NPP_MINSUM_". $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] ."').disabled = !this.checked;"
									),
									'DEFAULT' => 1,
								),

								'COMMISSION_NPP_PERCENT_'. $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] => array(
									'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_NPP_PERSENT"),
									'TYPE'    => 'STRING',
									'DEFAULT' => 2,
									'ATTRS'   => array(
										'disabled' => $disabled,
									),
								),

								'COMMISSION_NPP_MINSUM_'. $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] => array(
									'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_NPP_MINSUMM"),
									'TYPE'    => 'STRING',
									'DEFAULT' => 0,
									'ATTRS'   => array(
										'disabled' => $disabled,
									),
								),

								'COMMISSION_NPP_PAYMENT_'. $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] => array(
									'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_NPP_PAYMENT"),
									'TYPE'     => 'SELECT',
									'ITEMS'    => $arPayments,
									'MULTIPLE' => true,
									'HELP'     => '',
									'NULL'     => '',
								),

								'COMMISSION_NPP_DEFAULT_'. $arPersoneType['ID'] .'_'. $arPersoneType['SITE_ID'] => array(
									'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_NPP_DEFAULT"),
									'TYPE'     => 'CHECKBOX',
									'HELP'     => Loc::getMessage("IPOLH_DPD_OPTIONS_COMMISSION_NPP_DEFAULT_HELP"),
									'DEFAULT'  => 'Y',
								),
							),
						);
					}

					return $ret;
				}
			),
		),
	),

	array(
		"DIV" => "IPOLH_DPD_OPTIONS_TAB_SENDER",
		"TAB" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_SENDER"),
		"ICON" => "support_settings",
		"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_SENDER_TITLE"),
		"OPTIONS" => array(),
		'CONTROLS' => array(
			'SENDER_FIO' => array(
				'TYPE'  => 'STRING',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FIO"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FIO_HELP"),
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FIO_REQUIRED"),
				),
			),

			'SENDER_NAME' => array(
				'TYPE'  => 'STRING',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME_HELP"),
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME_REQUIRED"),
				),
			),

			'SENDER_PHONE' => array(
				'TYPE'  => 'STRING',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_PHONE"),
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_PHONE_REQUIRED"),
				),
			),

			'SENDER_EMAIL' => array(
				'TYPE'  => 'STRING',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_EMAIL"),
				"VALIDATORS" => array(
					function($field, $value, $form) {
						$values = $form->getEditItem();

						if ($values['SENDER_NEED_PASS'] == 'Y' && empty($value)) {
							return Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_EMAIL_REQUIRED");
						}
					}
				),
			),

			'SENDER_REGULAR_NUM' => array(
				'TYPE'  => 'STRING',
				'TITLE' => Loc::getMessage("IPOLG_DPD_OPTIONS_SENDER_REGULAR_NUM"),
			),

			'SENDER_NEED_PASS' => array(
				'TYPE'          => 'CHECKBOX',
				'TITLE'         => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NEED_PASS"),
				'VALUE'         => 'Y',
				'UNCHECK_VALUE' => 'N',
			),

			'SENDER_SUBHEADER_ADDRESS' => array(
				'TYPE'  => 'HEADER',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_ADDRESS_SUBHEADER"),
			),

			'SENDERS' => array(
				'SHOW_CAPTION' => 'N',
				'TYPE'         => function() {
					$data  = Option::get(IPOLH_DPD_MODULE, 'SENDERS', 'a:0:{}');
					$data  = unserialize($data) ?: [];
					$items = [];

					foreach ($data as $k => $values) {
						$arLocation = \Ipolh\DPD\DB\Location\Table::getByLocationId($values['LOCATION']);
						$values['LOCATION'] = implode(', ', array_filter(array_unique(array(
							'COUNTRY_NAME' => $arLocation['COUNTRY_NAME'],
							'REGION_NAME'  => $arLocation['REGION_NAME'],
							'CITY'         => $arLocation['CITY_NAME'],
						))));

						$values['DEFAULT'] = $values['DEFAULT'] == 'Y' ? '��' : '���';

						$items[] = [
							'id'   => $k,
							'data' => $values,
							'actions' => [
								[
									"ICONCLASS" => "edit",
									"TEXT"      => Loc::getMessage('IPOLH_DPD_OPTIONS_SENDER_EDIT'),
									"ONCLICK"   => 'ipolOptionsSenderFormHelper.open("'. $k .'");',
								],
							]
						];
					}

					ob_start();

					print '<div class="adm-toolbar-panel-container">';
					print '	<div class="adm-toolbar-panel-flexible-space">';
					print '		<input type="button" name="" value="'. Loc::getMessage('IPOLH_DPD_OPTIONS_SENDER_ADD') .'" id="b-ipol-dpd-options-sender-add">';
					print '	</div>';
					print '</div>';

					$GLOBALS['APPLICATION']->IncludeComponent('bitrix:main.ui.grid', '.default', [
						'GRID_ID'                   => IPOLH_DPD_MODULE .'_senders_table',
						'HEADERS'                   => [
							[
								'id'         => 'NAME', 
								'name'       => Loc::getMessage('IPOLH_DPD_OPTIONS_SENDER_NAME'),  
								'default'    => true, 
								'editable'   => false,
							],

							[
								'id'         => 'LOCATION', 
								'name'       => Loc::getMessage('IPOLH_DPD_OPTIONS_SENDER_LOCATION'),  
								'default'    => true, 
								'editable'   => false,
							],
			
							[
								'id'         => 'DEFAULT', 
								'name'       => Loc::getMessage('IPOLH_DPD_OPTIONS_SENDER_DEFAULT'),  
								'default'    => true, 
								'editable'   => false,
							],
						],
						'ROWS'                      => $items,
						'SHOW_ROW_CHECKBOXES'       => false,
						'SHOW_CHECK_ALL_CHECKBOXES' => false,
						'NAV_OBJECT'                => null,
						'AJAX_MODE'                 => false,
						'AJAX_ID'                   => '',
						'PAGE_SIZES'                => [],
						'SHOW_ROW_ACTIONS_MENU'     => true,
						'SHOW_GRID_SETTINGS_MENU'   => false,
						'SHOW_NAVIGATION_PANEL'     => false,
						'SHOW_PAGINATION'           => false,
						'SHOW_SELECTED_COUNTER'     => false,
						'SHOW_TOTAL_COUNTER'        => true,
						'SHOW_PAGESIZE'             => true,
						'SHOW_ACTION_PANEL'         => false,
						'ALLOW_SORT'                => false,
						'ALLOW_COLUMNS_SORT'        => false,
						'ALLOW_COLUMNS_RESIZE'      => true,
						'ALLOW_HORIZONTAL_SCROLL'   => true,
						'ALLOW_PIN_HEADER'          => true,
						'AJAX_OPTION_HISTORY'       => false,
						'EDITABLE'                  => false,
					]);

					return ob_get_clean();
				},
			),
		),
	),

	array(
		"DIV" => "IPOLH_DPD_OPTIONS_TAB_RECEIVER",
		"TAB" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_RECEIVER"),
		"ICON" => "",
		"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_RECEIVER_TITLE"),
		"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_RECEIVER_HELP"),
		"OPTIONS" => array(),
		"CONTROLS" => array(
			"IPOLH_DPD_ORDER_PROPERTY_TABS" => array(
				"TYPE"  => "TABS",
				"ITEMS" => function() use ($arPersoneTypes) {
					$tabs = array();
					$controlNames = array(
						'FIO' => true, 'NAME' => true, 'PHONE' => true, 'EMAIL' => true, 'LOCATION' => true, 'CITYALT' => false, 'STREET' => false, 'STREETABBR' => false, 
						'HOUSE' => false, 'KORPUS' => false, 'STR' => false, 'VLAD' => false, 'FLAT' => false, 'OFFICE' => false, 'PVZ_FIELD' => false
					);

					foreach ($arPersoneTypes as $arPersoneType) {
						$listItems = array('' => Loc::getMessage('IPOLH_DPD_ORDER_PROPERTY_CUSTOM'));
						$orderProps = \Ipolh\DPD\Utils::GetOrderProps($arPersoneType["ID"]);
						foreach($orderProps as $orderProp) {
							$code = $orderProp['CODE'] ?: 'PROP_'. $orderProp['ID'];
							$listItems[$code] = $orderProp['NAME'];
						}

						$controls = array();
						foreach ($controlNames as $controlName => $isRequired) {
							$controls['RECEIVER_'. $controlName .'_'. $arPersoneType["ID"]] = array(
								'TITLE'      => Loc::getMessage('IPOLH_DPD_OPTIONS_RECEIVER_'. $controlName),
								'TYPE'       => 'SELECT',
								'ITEMS'      => $listItems,
								'HELP'       => Loc::getMessage('IPOLH_DPD_OPTIONS_RECEIVER_'. $controlName .'_HELP'),
								'VALIDATORS' => $isRequired ? array(
									'required' => Loc::getMessage('IPOLH_DPD_OPTIONS_RECEIVER_'. $controlName .'_REQUIRED', ['#PERSONE_TYPE_NAME#' => $arPersoneType['NAME'], '#PERSON_TYPE_ID#' => $arPersoneType['ID']]),
								) : array(),
							);
						}

						$controls['RECEIVER_NEED_PASS_'. $arPersoneType['ID']] = array(
							'TYPE'          => 'CHECKBOX',
							'TITLE'         => Loc::getMessage('IPOLH_DPD_OPTIONS_RECEIVER_NEED_PASS'),
							'VALUE'         => 'Y',
							'UNCHECK_VALUE' => 'N',
						);

						$tabs[] = array(
							'DIV'      => sprintf('ptype_%s_%s', $arPersoneType['ID'], $arPersoneType['SITE_ID']),
							'TAB'      => sprintf('(%s) %s (%s)', $arPersoneType['ID'], $arPersoneType['NAME'], $arPersoneType['SITE_ID']),
							'TITLE'    => '',
							'CONTROLS' => $controls,
						);
					}

					return $tabs;
				},
			),
		),
	),

	array(
		'DIV'      => "IPOLH_DPD_OPTIONS_TAB_OPTIONS",
		'TAB'      => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_OPTIONS"),
		'ICON'     => '',
		'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_OPTIONS_TITLE"),
		'OPTIONS'  => array(),
		'CONTROLS' => array(
			"SELF_PICKUP" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_SELF_PICKUP"),
				"TYPE" => "SELECT",
				"ITEMS" => array(
					1 => Loc::getMessage("IPOLH_DPD_OPTIONS_SELF_PICKUP_YES"),
					0 => Loc::getMessage("IPOLH_DPD_OPTIONS_SELF_PICKUP_NO"),
				),
				"DEFAULT" => 1,
				"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_SELF_PICKUP_HELP"),
			),

			"PAYMENT_TYPE" => array(
				"TITLE" => Loc::getMessage("IPOLH_DPD_OPTIONS_PAYMENT_TYPE"),
				"TYPE" => "SELECT",
				"ITEMS" => array(
					''    => Loc::getMessage("IPOLH_DPD_OPTIONS_PAYMENT_TYPE_AUTO"),
					'OUP' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAYMENT_TYPE_OUP"),
					'OUO' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAYMENT_TYPE_OUO"),
				),
				"DEFAULT" => '',
				"HELP" => Loc::getMessage("IPOLH_DPD_OPTIONS_PAYMENT_TYPE_HELP"),
			),

			'PICKUP_TIME_PERIOD' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD"),
				'TYPE'  => 'SELECT',
				'ITEMS' => array(
					'9-18'  => Loc::getMessage("IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_9_18"),
					'9-13'  => Loc::getMessage("IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_9_13"),
					'13-18' => Loc::getMessage("IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_13_18"),
				),
				'DEFAULT' => '9-18',
			),

			'DELIVERY_TIME_PERIOD' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD"),
				'TYPE'  => 'SELECT',
				'ITEMS' => array(
					'9-18'  => Loc::getMessage("IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_9_18"),
					'9-14'  => Loc::getMessage("IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_9_14"),
					'13-18' => Loc::getMessage("IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_13_18"),
					'18-22' => Loc::getMessage("IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_18_22"),
				),
				'DEFAULT' => '9-18',
			),

			'CARGO_NUM_PACK' => array(
				'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_CARGO_NUM_PACK"),
				'TYPE'    => 'STRING',
				'DEFAULT' => 1,
			),

			'CARGO_CATEGORY' => array(
				'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_CARGO_CATEGORY"),
				'TYPE'    => 'STRING',
				'DEFAULT' => Loc::getMessage("IPOLH_DPD_OPTIONS_CARGO_CATEGORY_DEFAULT"),
				"VALIDATORS" => array(
					"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_CARGO_CATEGORY_REQUIRED"),
				),
			),

			'SUBHEAER_OPTIONS_OPTIONS' => array(
				'TYPE'  => 'HEADER',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_OPTIONS_SUBTAB_OPTIONS"),
			),

			'OPTIONS_OPTIONS_HELP' => array(
				'TYPE' => 'COMMENT',
				'HELP' => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_OPTIONS_HELP"),
			),

			'CARGO_REGISTERED' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_CARGO_REGISTERED"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_CARGO_REGISTERED_HELP"),
				'VALUE' => 'Y',
				'UNCHECK_VALUE' => 'N',
			),

			'DVD' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_DVD"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_DVD_HELP"),
				'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAID_COMMENT"),			
				'VALUE' => 'Y',
				'UNCHECK_VALUE' => 'N',
			),

			'TRM' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_TRM"),
				'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAID_COMMENT"),
				'VALUE' => 'Y',
				'UNCHECK_VALUE' => 'N',
			),

			'PRD' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_PRD"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_PRD_HELP"),
				'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAID_COMMENT"),
				'VALUE' => 'Y',
				'UNCHECK_VALUE' => 'N',
			),

			'VDO' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_VDO"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_VDO_HELP"),
				'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAID_COMMENT"),
				'VALUE' => 'Y',
				'UNCHECK_VALUE' => 'N',
			),

			'OGD' => array(
				'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD"),
				'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAID_COMMENT"),
				'TYPE'    => 'SELECT',
				'ITEMS'   => array(
					'' => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_EMPTY"),
					Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PRIM") => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PRIM_TITLE"),
					Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PROS") => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PROS_TITLE"),
					Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_RAB") => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_RAB_TITLE"),
				),
			),

			'CHST' => array(
				'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_CHST"),
				// 'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_CHST_COMMENT"),
				'TYPE'    => 'SELECT',
				'ITEMS'   => array(
					'' => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_EMPTY"),
					Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PRIM") => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PRIM_TITLE"),
					Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PROS") => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_PROS_TITLE"),
					Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_RAB") => Loc::getMessage("IPOLH_DPD_OPTIONS_OGD_RAB_TITLE"),
				),
			),

			'GOODS_RETURN_AMOUNT' => array(
				'TITLE'   => Loc::getMessage('IPOLH_DPD_OPTIONS_GOODS_RETURN_AMOUNT'),
				// 'COMMENT' => Loc::getMessage('IPOLH_DPD_OPTIONS_GOODS_RETURN_AMOUNT_COMMENT'),
				'TYPE'    => 'STRING',
			),
			
			'DELIVERY_AMOUNT' => array(
				'TITLE'   => Loc::getMessage('IPOLH_DPD_OPTIONS_DELIVERY_AMOUNT'),
				// 'COMMENT' => Loc::getMessage('IPOLH_DPD_OPTIONS_DELIVERY_AMOUNT_COMMENT'),
				'TYPE'    => 'STRING',
			),

			'SUBHEADER_OPTIONS_NOTIFY' => array(
				'TYPE'  => 'HEADER',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_OPTIONS_SUBTAB_NOTIFY"),
			),

			'SMS' => array(
				'TYPE'  => 'HIDDEN',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SMS"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SMS_HELP"),
			),

			'EML' => array(
				'TYPE'  => 'HIDDEN',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_EML"),
			),

			'ESD' => array(
				'TYPE'  => 'HIDDEN',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_ESD"),
			),

			'ESZ' => array(
				'TYPE'  => 'STRING',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_ESZ"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_ESZ_HELP"),
			),

			'POD' => array(
				'TYPE'  => 'HIDDEN',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_POD"),
				'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_POD_HELP"),
				'COMMENT' => Loc::getMessage("IPOLH_DPD_OPTIONS_PAID_COMMENT"),
			),
		),
	),

	array(
		'DIV'      => "IPOLH_DPD_OPTIONS_TAB_STATUS",
		'TAB'      => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_STATUS"),
		'ICON'     => '',
		'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_STATUS_TITLE"),
		'HELP'     => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_STATUS_HELP"),
		'CONTROLS' => array(
			'SET_TRACKING_NUMBER' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SET_TRACKING_NUMBER"),
				'DEFAULT' => 0,
			),

			'MARK_PAYED' => array(
				'TYPE'  => 'CHECKBOX',
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_MARK_PAYED"),
				'HELP' => Loc::getMessage("IPOLH_DPD_OPTIONS_MARK_PAYED_HELP")
			),

			'STATUS_ORDER_CHECK' => array(
				'TYPE'     => 'checkbox',
				'TITLE'    => Loc::getMessage('IPOLH_DPD_OPTIONS_STATUS_ORDER_CHECK'),
				'DEFAULT'  => 0,
				'ATTRS'    => array(
					'onchange' => ""
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_PICKUP').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_TRANSIT').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_READY').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_READY_PICKUP').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_COURIER').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_DELIVERED').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_CANCEL').disabled = !this.checked;"
						. "BX('IPOLH_DPD_OPTIONS_STATUS_ORDER_PROBLEM').disabled = !this.checked;"
					,
				),
			),

			'STATUS_ORDER_PICKUP' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_PICKUP"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_TRANSIT' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_TRANSIT"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_READY_PICKUP' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_READY_PICKUP"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_READY' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_READY"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_COURIER' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_COURIER"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_DELIVERED' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_DELIVERED"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_PROBLEM' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_PROBLEM"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),

			'STATUS_ORDER_CANCEL' => array(
				'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_CANCEL"),
				'TYPE'  => 'SELECT',
				'ITEMS' => $arOrderStatuses,
				'NULL'  => Loc::getMessage("IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"),
				'ATTRS' => array(
					'disabled' => !Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK', 0),
				),
			),
		),
	),
    array(
        'DIV'      => "IPOLH_DPD_OPTIONS_TAB_FAQ",
        'TAB'      => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_FAQ"),
        'ICON'     => '',
//        'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_TAB_FAQ_TITLE"),
        'CONTROLS' => array(
            'FAQ' => array(
                'TITLE'        => '',
                'SHOW_CAPTION' => 'N',
                'TYPE'         => function() {
                    return Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_FAQ_HELP');
                },
            ),
			'CHECKER' => array(
                'TITLE'        => '',
                'SHOW_CAPTION' => 'N',
                'TYPE'         => function() {
                    return Loc::getMessage('IPOLH_DPD_OPTIONS_TAB_FAQ_CHECKER');
                },
            ),
        ),
    ),
);

$formEdit = new \Ipolh\DPD\Admin\Options\Edit(IPOLH_DPD_MODULE, $aTabs);
$formEdit->processAndShow();

if (\Ipolh\DPD\API\User::isActiveAccount()) {
	$formServices = new \Ipolh\DPD\Admin\Options\Services(IPOLH_DPD_MODULE);
	print $formServices->render();
}

print '<div style="display: none">';
	$form = new \Ipolh\DPD\Admin\Options\Sender();
	$form->render();
print '</div>';