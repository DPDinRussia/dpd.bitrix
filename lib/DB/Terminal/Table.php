<?php
namespace Ipolh\DPD\DB\Terminal;

use \Bitrix\Main;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Ipolh\DPD\Shipment;

Loc::loadMessages(__FILE__);

class Table extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'b_ipol_dpd_terminal';
	}

	public static function getMap()
	{
		return [
			new Entity\IntegerField('ID', [
				'primary' => true,
				'autocomplete' => true,
			]),

			new Entity\StringField('LOCATION_ID', [
				'required' => false,
			]),

			new Entity\StringField('CODE', [
				'required' => false,
			]),

			new Entity\StringField('NAME', [
				'required' => false,
			]),

			new Entity\StringField('ADDRESS_FULL', [
				'required' => false,
			]),

			new Entity\StringField('ADDRESS_SHORT', [
				'required' => false,
			]),

			new Entity\StringField('ADDRESS_DESCR', [
				'required' => false,
			]),

			new Entity\StringField('PARCEL_SHOP_TYPE', [
				'required' => false,
			]),

			new Entity\StringField('SCHEDULE_SELF_PICKUP', [
				'required' => false,
			]),

			new Entity\StringField('SCHEDULE_SELF_DELIVERY', [
				'required' => false,
			]),

			new Entity\StringField('SCHEDULE_PAYMENT_CASH', [
				'required' => false,
			]),
			
			new Entity\StringField('SCHEDULE_PAYMENT_CASHLESS', [
				'required' => false,
			]),

			new Entity\TextField('SCHEDULE_PAYMENTS', [
				'required' => false,
				'serialized' => true,
			]),

			new Entity\FloatField('LATITUDE', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LONGITUDE', [
				'default_value' => 0,
			]),

			new Entity\BooleanField('IS_LIMITED', [
				'values' => array('N', 'Y'),
				'default_value' => 'N',
			]),

			new Entity\FloatField('LIMIT_MAX_SHIPMENT_WEIGHT', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LIMIT_MAX_WEIGHT', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LIMIT_MAX_LENGTH', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LIMIT_MAX_WIDTH', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LIMIT_MAX_HEIGHT', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LIMIT_MAX_VOLUME', [
				'default_value' => 0,
			]),

			new Entity\FloatField('LIMIT_SUM_DIMENSION', [
				'default_value' => 0,
			]),

			new Entity\FloatField('NPP_AMOUNT', [
				'default_value' => 0,
			]),

			new Entity\BooleanField('NPP_AVAILABLE', [
				'values' => array('N', 'Y'),
				'default_value' => 'N',
			]),
			
			new Entity\BooleanField('UPDATE_CHECKED', [
				'values' => array('N', 'Y'),
				'default_value' => 'N',
			]),

			new Entity\StringField('SERVICES', [
				'default_value'           => '',
				'save_data_modification'  => function() {
					return [
						function ($value) {
							if (is_array($value)) {
								$value = '|'. implode('|', $value) .'|';
							}

							return $value;
						}
					];
				},
				'fetch_data_modification' => function() {
					return [
						function ($value) {
							if (!is_array($value)) {
								$value = explode('|', trim($value, '|'));
							}

							return $value;
						}
					];
				}
			])
		];
	}

	public static function getList(array $parms = array())
	{
		$parms = static::prepareFilter($parms);

		return parent::getList($parms);
	}

	public static function getCount($filter = [], array $cache = []) {
        $filter = static::prepareFilter(['filter' => $filter]);

		return parent::getCount($filter['filter']);
	}

	/**
	 * ���������� ������ �� �������������� ��������
	 * 
	 * @param  int $locationId
	 * @param  array  $select
	 * @return array|false
	 */
	public static function getByLocationId($locationId, $select = array())
	{	
		return static::getList(array_filter([
			'select' => $select ?: null,
			'filter' => ['LOCATION_ID' => $arBxLocation['ID']]
		]));
	}

	public static function getByCode($code, $select = array())
	{
		return static::getList(array_filter([
			'select' => $select ?: null,
			'filter' => ['CODE' => $code]
		]))->fetch();
	}

	protected static function prepareFilter($parms)
	{
		$parms = static::addConfigFilter($parms);
		$parms = static::addShipmentFilter($parms);

		return $parms;
	}

	protected static function addShipmentFilter($parms)
	{
		if (!isset($parms['filter']['SHIPMENT'])) {
			return $parms;
		}

		if ($parms['filter']['SHIPMENT'] instanceof Shipment) {
			$shipment            = $parms['filter']['SHIPMENT'];
			$location            = $shipment->getReceiver();
			$isPaymentOnDelivery = isset($parms['filter']['PAYMENT_ON_DELIVERY']) 
				? $parms['filter']['PAYMENT_ON_DELIVERY'] 
				: $shipment->isPaymentOnDelivery()
			;

			$isSelfDelivery = isset($parms['filter']['IS_SELF_DELIVERY'])
				? $parms['filter']['IS_SELF_DELIVERY']
				: $shipment->getSelfDelivery()
			;

			$dimensions = [$shipment->getWidth(), $shipment->getHeight(), $shipment->getLength()];
			sort($dimensions);

			$currencyFrom = $shipment->getCurrency();
			$currencyTo   = \Ipolh\DPD\API\User::getInstance()->getClientCurrency();
			$nppAmount    = \CCurrencyRates::ConvertCurrency($shipment->getPrice(), $currencyFrom, $currencyTo);

			$parms['filter'] = array_merge(
				$parms['filter'],

				[
					'LOCATION_ID' => $location['ID'],
					
					[
						'LOGIC' => 'OR',
						['!IS_LIMITED' => 'Y'],
						[						
							[
								'LOGIC' => 'OR',
								['<=LIMIT_MAX_WIDTH' => 0],
								['>=LIMIT_MAX_WIDTH' => $dimensions[0]]
							],

							[
								'LOGIC' => 'OR',
								['<=LIMIT_MAX_HEIGHT' => 0],
								['>=LIMIT_MAX_HEIGHT' => $dimensions[1]]
							],

							[
								'LOGIC' => 'OR',
								['<=LIMIT_MAX_LENGTH' => 0],
								['>=LIMIT_MAX_LENGTH' => $dimensions[2]]
							],

							[
								'LOGIC' => 'OR',
								['<=LIMIT_MAX_WEIGHT' => 0],
								['>=LIMIT_MAX_WEIGHT' => $shipment->getWeight()]
							],

							[
								'LOGIC' => 'OR',
								['<=LIMIT_MAX_VOLUME' => 0],
								['>=LIMIT_MAX_VOLUME' => $shipment->getVolume()]
							],

							[
								'LOGIC' => 'OR',
								['<=LIMIT_SUM_DIMENSION' => 0],
								['>=LIMIT_SUM_DIMENSION' => array_sum([$shipment->getWidth(), $shipment->getHeight(), $shipment->getLength()])]
							],
						]
					],
				],

				$isSelfDelivery
					? ['!SCHEDULE_SELF_DELIVERY' => false]
					: []
				,

				$isPaymentOnDelivery
					? ['NPP_AVAILABLE' => 'Y', '>=NPP_AMOUNT' => $nppAmount]
					: []
				,

				[]
			);
		}

		unset($parms['filter']['SHIPMENT']);
		unset($parms['filter']['PAYMENT_ON_DELIVERY']);

		return $parms;
	}

	protected static function addConfigFilter($parms)
	{
		if (!isset($parms['filter']['USE_CONFIG'])) {
			return $parms;
		}

		if ($parms['filter']['USE_CONFIG']) {
			$trm       = Option::get(IPOLH_DPD_MODULE, 'TRM', 'N') == 'Y';
			$ogd       = Option::get(IPOLH_DPD_MODULE, 'OGD', '');
			
			if ($trm || $ogd) {
				$subFilter = ['LOGIC' => 'AND'];
				
				if ($trm) {
					$subFilter[] = ['SERVICES' => '%|'. Loc::getMessage('IPOLH_DPD_OPTIONS_TRM_CODE') .'|%'];
				}
	
				if ($ogd) {
					$subFilter[] = ['SERVICES' => '%|'. Loc::getMessage('IPOLH_DPD_OPTIONS_OGD_CODE') .'_'. $ogd .'|%'];
				}
	
				$parms['filter'][] = [
					[
						'LOGIC'    => 'OR',
						'SERVICES' => false,
						$subFilter,
					]
				];
			}
		}

		$disableParcelShopTypes = Option::get(IPOLH_DPD_MODULE, "TYPE_DISPLAYED_PVZ", array());
		$disableParcelShopTypes = array_filter((array) unserialize ($disableParcelShopTypes)) ?: array();

		if (in_array(Loc::getMessage('IPOLH_DPD_OPTIONS_TERMINAL_TYPE_PVP_KEY'), $disableParcelShopTypes)) {
			$disableParcelShopTypes[] = '';
		}

		if ($disableParcelShopTypes) {
			$parms['filter']['!PARCEL_SHOP_TYPE'] = $disableParcelShopTypes;
		}

		unset($parms['filter']['USE_CONFIG']);

		return $parms;
	}
}