<?php
namespace Ipolh\DPD\DB\Order;

use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Result;
use \Bitrix\Main\Error;
use \Bitrix\Main\SystemException;

use \Ipolh\DPD\Utils;
use \Ipolh\DPD\Order as DpdOrder;
use \Ipolh\DPD\DB\AbstractModel;
use \Ipolh\DPD\API\User as API;
use \Ipolh\DPD\TerminalsManager;
use \Ipolh\DPD\Delivery\DPD;

Loc::loadMessages(__FILE__);

class Model extends AbstractModel
{
	/**
	 * Поля битриксового заказа
	 * @var array
	 */
	protected $orderFields;

	/**
	 * Св-ва битриксового заказа
	 * @var array
	 */
	protected $orderProps;

	/**
	 * Состав битриксового заказа
	 * @var array
	 */
	protected $orderItems;

	/**
	 * Отправление
	 * @var \Ipolh\DPD\Shipment
	 */
	protected $shipment;

	/**
	 * @return string
	 */
	public static function DataManager()
	{
		return '\\Ipolh\\DPD\\DB\\Order\\Table';
	}

	/**
	 * Возвращает список статусов и их описаний
	 */
	public static function StatusList()
	{
		return array(
			DpdOrder::STATUS_NEW => Loc::getMessage('DPD_ORDER_STATUS_NEW'),
			DpdOrder::STATUS_PENDING => Loc::getMessage('DPD_ORDER_STATUS_PENDING'),
			DpdOrder::STATUS_OFFER_CREATE => 'Получена заявка',
			DpdOrder::STATUS_OFFER_ERROR => '​В заявке присутствует ошибка',
			DpdOrder::STATUS_OFFER_WAITING => '​Запрошены паспортные данные получателя',
			DpdOrder::STATUS_OFFER_CANCEL => 'Отмена заявки',
			DpdOrder::STATUS_OK => Loc::getMessage('DPD_ORDER_STATUS_OK'),
			DpdOrder::STATUS_WAITING => 'Заказ ожидает дату приема',
			DpdOrder::STATUS_ERROR            => Loc::getMessage('DPD_ORDER_STATUS_ERROR'),
			DpdOrder::STATUS_DEPARTURE => Loc::getMessage('DPD_ORDER_STATUS_DEPARTURE'),
			DpdOrder::STATUS_TRANSIT => Loc::getMessage('DPD_ORDER_STATUS_TRANSIT'),
			DpdOrder::STATUS_TRANSIT_TERMINAL => Loc::getMessage('DPD_ORDER_STATUS_TRANSIT_TERMINAL'),
			DpdOrder::STATUS_ARRIVE_PICKUP => Loc::getMessage('DPD_ORDER_STATUS_ARRIVE_PICKUP'),
			DpdOrder::STATUS_ARRIVE_COURIER => Loc::getMessage('DPD_ORDER_STATUS_ARRIVE'),
			DpdOrder::STATUS_COURIER => Loc::getMessage('DPD_ORDER_STATUS_COURIER'),
			
			DpdOrder::STATUS_TRANSIT_RETURN => 'Заказ следует по маршруту до терминала возврата',
			DpdOrder::STATUS_ARRIVE_PICKUP_RETURN => 'Заказ на возврат готов к выдаче',
			DpdOrder::STATUS_ARRIVE_COURIER_RETURN => '​Заказ на возврат готов к передаче курьеру для доставки',
			DpdOrder::STATUS_COURIER_RETURN => 'Посылка передана курьеру для возврата',

			DpdOrder::STATUS_CUSTOMS_CLEARANCE => 'Таможенное оформление в стране отправления',
			DpdOrder::STATUS_END_CUSTOMS_CLEARANCE => '​Закончено таможенное оформление в стране отправления',
			DpdOrder::STATUS_ARRIVED_IN_RF => 'Заказ прибыл в страну доставки',
			DpdOrder::STATUS_END_CUSTOMS_CLEARANCE_IN_RF => 'Закончено таможенное оформление',
			DpdOrder::STATUS_TRANSIT_SPEC => 'Передано спецперевозчику',

			DpdOrder::STATUS_PROBLEM => Loc::getMessage('DPD_ORDER_STATUS_PROBLEM'),
			DpdOrder::STATUS_DELIVERY_PROBLEM => 'Отказ от заказа в момент доставки',
			DpdOrder::STATUS_NOT_DONE => Loc::getMessage('DPD_ORDER_STATUS_NOT_DONE'),
			DpdOrder::STATUS_CANCEL => Loc::getMessage('DPD_ORDER_STATUS_CANCEL'),
			DpdOrder::STATUS_CANCEL_PREV      => Loc::getMessage('DPD_ORDER_STATUS_CANCEL_PREV'),
			DpdOrder::STATUS_REMOVED => '​Заказ утилизирован',
			DpdOrder::STATUS_NOT_CLAIMED => '​Посылка не востребована',
			DpdOrder::STATUS_LOST => Loc::getMessage('DPD_ORDER_STATUS_LOST'),
			DpdOrder::STATUS_DELIVERED => Loc::getMessage('DPD_ORDER_STATUS_DELIVERED'),
			DpdOrder::STATUS_RETURNED => Loc::getMessage('DPD_ORDER_STATUS_RETURNED'),
			DpdOrder::STATUS_NEW_DPD          => Loc::getMessage('DPD_ORDER_STATUS_NEW_DPD'),
			DpdOrder::STATUS_NEW_CLIENT       => Loc::getMessage('DPD_ORDER_STATUS_NEW_CLIENT'),
		);
	}

	/**
	 * Получаем информацию по заказу
	 * ассоциированному с записью
	 *
	 * @return void
	 */
	public function afterLoad()
	{
		$this->loadOrder();
	}

	/**
	 * Заполняет поля на основе данных заказа
	 *
	 * @param mixed $order ID или массив заказа
	 * @return void
	 */
	public function fillFromOrder($order)
	{
		$this->setOrder($order);

		$this->orderDate        = $this->orderFields['DATE_INSERT'];
		$this->price            = $this->orderFields['PRICE'] - $this->orderFields['PRICE_DELIVERY'];
		$this->priceDelivery    = $this->orderFields['PRICE_DELIVERY'];
		$this->receiverLocation = $this->orderProps['IPOLH_DPD_LOCATION'];
		$this->receiverComment  = $this->orderFields['USER_DESCRIPTION'];

		$address_fields = array('FIO', 'NAME', 'PHONE', 'EMAIL', 'LOCATION', 'STREET',  'STREETABBR', 'HOUSE', 'KORPUS', 'STR', 'VLAD', 'OFFICE', 'FLAT', 'NEED_PASS');
		foreach ($address_fields as $field) {
			$fname = 'RECEIVER_'. $field;
			$code  = Option::get(IPOLH_DPD_MODULE, $fname .'_'. $this->orderFields['PERSON_TYPE_ID']);
			$value = isset($this->orderProps[$code]) ? $this->orderProps[$code] : '';

			if ($field == 'NEED_PASS') {
				$value = $value === 'N' || !$value ? 'N' : 'Y';
			}

			$this->$fname = $value;
		}

		if (($senderIndex = Utils::getSenderIndexByLocation($this->receiverLocation)) !== false) {
			$this->sender = $senderIndex;
		}

		$shipment = $this->getShipment(true);

		$this->npp             = $shipment->isPaymentOnDelivery($this->receiverTerminalCode) && $this->orderFields['PAYED'] != 'Y' ? 'Y' : 'N';
		$this->useCargoValue   = $shipment->getDeclaredValue() ? 'Y' : 'N';

		$this->dimensionWidth  = $shipment->getWidth();
		$this->dimensionHeight = $shipment->getHeight();
		$this->dimensionLength = $shipment->getLength();
		$this->cargoVolume     = $shipment->getVolume();
		$this->cargoWeight     = $shipment->getWeight();
		$this->reloadUnits();
		
		$deliveryCode = DPD::getDeliveryCode($this->orderFields['DELIVERY_ID']);
		$profile      = DPD::getDeliveryProfile($deliveryCode);

		if ($this->getServiceVariant() != $this->setServiceVariant($profile)->getServiceVariant()) {		
			$shipment          = $this->getShipment(true);
			$tariff            = $shipment->calculator()->calculate();
			$this->serviceCode = $tariff['SERVICE_CODE'];
		}
	}

	/**
	 * Заполняет поля на основе настроек модуля
	 *
	 * @return void
	 */
	public function fillFromConfig()
	{
		$this->SENDER = Utils::getSenderIndexDefault();

		$fields = array('PICKUP_TIME_PERIOD', 'DELIVERY_TIME_PERIOD', 'CARGO_CATEGORY', 'CARGO_NUM_PACK',
			'SENDER_FIO', 'SENDER_NAME', 'SENDER_PHONE', 'SENDER_EMAIL', 'SENDER_NEED_PASS', 'CARGO_REGISTERED',
			'DVD', 'TRM', 'PRD', 'VDO', 'OGD', 'CHST', 'GOODS_RETURN_AMOUNT', 'DELIVERY_AMOUNT', 'SMS', 'EML', 'ESD', 'ESZ', 'POD', 'PAYMENT_TYPE',
		);

		foreach ($fields as $field) {
			$value = Option::get(IPOLH_DPD_MODULE, $field, '');

			if ($field == 'SENDER_NEED_PASS') {
				$value = $value === 'N' || !$value ? 'N' : 'Y';
			}

			$this->$field = $value;
		}
	}

	/**
	 * Возвращает отправку
	 *
	 * @return \Ipolh\DPD\Shipment
	 */
	public function getShipment($forced = false)
	{
		if (is_null($this->shipment) || $forced) {
			$this->shipment = new \Ipolh\DPD\Shipment();
			$this->shipment->setSender($this->senderLocation);
			$this->shipment->setReceiver($this->receiverLocation);
			$this->shipment->setPaymentMethod($this->orderFields['PERSON_TYPE_ID'], $this->orderFields['PAY_SYSTEM_ID']);
			$this->shipment->setItems($this->orderItems, $this->cargoValue);

			list($selfPickup, $selfDelivery) = array_values($this->getServiceVariant());
			$this->shipment->setSelfPickup($selfPickup);
			$this->shipment->setSelfDelivery($selfDelivery);

			if ($this->id > 0) {
				$this->shipment->setWidth($this->dimensionWidth);
				$this->shipment->setHeight($this->dimensionHeight);
				$this->shipment->setLength($this->dimensionLength);
				$this->shipment->setWeight($this->cargoWeight);
			}
		}

		return $this->shipment;
	}

	/**
	 * Устанавливает ID заказа битрикса
	 *
	 * @param string $orderId
	 */
	public function setOrderId($orderId)
	{
		$this->fields['ORDER_ID'] = $orderId;
		$this->loadOrder();
	}

	/**
	 * Устанавливает параметры заказа
	 * 
	 * @param array $order
	 *
	 * @return void
	 */
	public function setOrder($order)
	{
		if (is_array($order)) {	
			$this->fields['ORDER_ID'] = \Ipolh\DPD\Utils::getOrderId($order);
			$this->orderFields        = $order;

			$this->loadOrderProps();
			$this->loadOrderItems();

			return;
		}

		if ($order instanceof \Bitrix\Sale\Order) {
			$this->orderFields        = $order->getFields()->getValues();
			$this->fields['ORDER_ID'] = \Ipolh\DPD\Utils::getOrderId($this->orderFields);

			foreach ($order->getPropertyCollection() as $property) {
				$code  = $property->getField('CODE') ?: 'PROP_'. $property->getPropertyId();
				$value = $property->getField('VALUE');

				$this->orderProps[$code] = $value;
			}

			foreach ($order->getBasket() as $item) {
				$item = $item->getFields()->getValues();
				$this->orderItems[$item['PRODUCT_ID']] = $item;
			}

			return;
		}

		$this->setOrderId($order);
	}

	public function setServiceCode($value)
	{
		$this->fields['SERVICE_CODE'] = $value;

		if ($value == 'DAY') {
			$this->setPickupDate(date('d.m.Y'));
		}

		return $this;
	}

	/**
	 * Устанавливает вариант доставки
	 *
	 * @param string $variant
	 */
	public function setServiceVariant($variant)
	{
		$D = Loc::getMessage('DPD_ORDER_SERVICE_VARIANT_D');
		$T = Loc::getMessage('DPD_ORDER_SERVICE_VARIANT_T');

		if (is_string($variant) && preg_match('~^('. $D .'|'. $T .'){2}$~sUi', $variant)) {
			$this->fields['SERVICE_VARIANT'] = $variant;
			return;
		}

		if (is_array($variant)) {
			$selfPickup   = $variant['SELF_PICKUP'];
			$selfDelivery = $variant['SELF_DELIVERY'];
		} else {
			$selfPickup   = Option::get(IPOLH_DPD_MODULE, 'SELF_PICKUP');
			$selfDelivery = $variant == 'PICKUP';
		}

		$this->fields['SERVICE_VARIANT'] = ''
			. ($selfPickup   ? $T : $D)
			. ($selfDelivery ? $T : $D)
		;

		return $this;
	}

	/**
	 * Возвращает вариант доставки
	 *
	 * @return array
	 */
	public function getServiceVariant()
	{
		$D = Loc::getMessage('DPD_ORDER_SERVICE_VARIANT_D');
		$T = Loc::getMessage('DPD_ORDER_SERVICE_VARIANT_T');

		return array(
			'SELF_PICKUP'   => mb_substr($this->fields['SERVICE_VARIANT'], 0, 1) == $T,
			'SELF_DELIVERY' => mb_substr($this->fields['SERVICE_VARIANT'], 1, 1) == $T,
		);
	}

	public function isSelfPickup()
	{
		$serviceVariant = $this->getServiceVariant();
		return $serviceVariant['SELF_PICKUP'];
	}

	public function isSelfDelivery()
	{
		$serviceVariant = $this->getServiceVariant();
		return $serviceVariant['SELF_DELIVERY'];
	}

	/**
	 * Возвращает текстовое описание статуса заказа
	 *
	 * @return string
	 */
	public function getOrderStatusText()
	{
		$statusList = static::StatusList();
		$ret = $statusList[$this->orderStatus];

		if ($this->orderStatus == DpdOrder::STATUS_ERROR) {
			$ret .= ': '. $this->orderError;
		}

		return $ret;
	}

	/**
	 * Возвращает текстовое представление местоположения отправителя
	 *
	 * @return string
	 */
	public function getSenderLocationText()
	{
		$arLocation = $this->getShipment()->getSender();

		return implode(', ', array_filter(array_unique(array(
			'COUNTRY_NAME' => $arLocation['COUNTRY_NAME'],
			'REGION_NAME'  => $arLocation['REGION_NAME'],
			'CITY'         => $arLocation['CITY_NAME'],
		))));
	}

	/**
	 * Возвращает текстовое представление местоположения получателя
	 *
	 * @return string
	 */
	public function getReceiverLocationText()
	{
		$arLocation = $this->getShipment()->getReceiver();

		return implode(', ', array_filter(array_unique(array(
			'COUNTRY_NAME' => $arLocation['COUNTRY_NAME'],
			'REGION_NAME'  => $arLocation['REGION_NAME'],
			'CITY'         => $arLocation['CITY_NAME'],
		))));
	}

	/**
	 * Возвращает ифнормацию о тарифе
	 *
	 * @param  boolean $forced пересоздать ли экземпляр отгрузки
	 *
	 * @return \Bitrix\Main\Result
	 */
	public function getTariffDelivery($forced = false)
	{
		$result = new Result();

		$tariff = $this->getShipment($forced)->calculator()->calculateWithTariff($this->serviceCode, $this->currency);
		if (!$tariff) {
			$result->addError(new Error(Loc::getMessage('IPOLH_DPD_ORDER_GET_TARIFF_ERROR')));
		} else {
			$result->setData($tariff);
		}

		return $result;
	}

	/**
	 * Возвращает стоимость доставки в заказе
	 *
	 * @return float
	 */
	public function getActualPriceDelivery()
	{
		$result = $this->getTariffDelivery();
		if ($result->isSuccess()) {
			$tariff = $result->getData();

			return $tariff['COST'];
		}

		return false;
	}

	/**
	 * Возвращает оплаченную сумму заказа
	 * @return float
	 */
	public function getPayedPrice()
	{
		return $this->orderFields['SUM_PAID'];
	}

	/**
	 * Возвращает валюту заказа
	 *
	 * @return string
	 */
	public function getCurrency()
	{
		return $this->orderFields['CURRENCY'];
	}

	/**
	 * Сеттер для номера заказа, попутно устанавливаем номер отправления
	 *
	 * @param $orderNum
	 */
	public function setOrderNum($orderNum)
	{
		$this->fields['ORDER_NUM']             = $orderNum;
		$this->fields['ORDER_DATE_CREATE']     = $orderNum ? \ConvertTimeStamp(false, "FULL") : '';
		$this->fields['ORDER_DATETIME_CREATE'] = $orderNum ? new \Bitrix\Main\Type\DateTime() : null;

		if (!empty($orderNum)
			&& Option::get(IPOLH_DPD_MODULE, 'SET_TRACKING_NUMBER')
		) {
			\CSaleOrder::Update($this->orderFields['ID'], array(
				'TRACKING_NUMBER' => $this->fields['ORDER_NUM'],

				// 'DELIVERY_DOC_NUM'  => $this->fields['ORDER_NUM'],
				// 'DELIVERY_DOC_DATE' => $this->fields['ORDER_DATE_CREATE'],
			));

			$order    = \Bitrix\Sale\Order::load($this->orderFields['ID']);
			$shipment = $order->getShipmentCollection()[0];

			if ($shipment) {
				$shipment->setField('TRACKING_NUMBER', $this->fields['ORDER_NUM']);
				$shipment->save();
			}
		}
	}

	/**
	 * Сеттер для статуса заказа
	 * попутно выставляем статус битриксового заказа
	 */
	public function setOrderStatus($orderStatus, $orderStatusDate = false)
	{
		if (empty($orderStatus)) {
			return;
		}

		if (!array_key_exists($orderStatus, self::StatusList())) {
			return;
		}

		if ($this->isSelfDelivery() 
			&& in_array($orderStatus, [DpdOrder::STATUS_COURIER])
		) {
			return;
		}

		$this->fields['ORDER_STATUS'] = $orderStatus;
		$this->fields['ORDER_DATE_STATUS'] = $orderStatusDate ?: \ConvertTimeStamp(false, "FULL");

		if (in_array($orderStatus, [DpdOrder::STATUS_CANCEL, DpdOrder::STATUS_OFFER_CANCEL, DpdOrder::STATUS_CANCELLED])) {
			$this->fields['ORDER_DATE_CANCEL'] = $orderStatusDate ?: \ConvertTimeStamp(false, "FULL");
		}

		if (!empty($orderStatus)
			&& (Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CHECK'))
		) {
			$status = array(
				DpdOrder::STATUS_DEPARTURE        => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_PICKUP'),
				DpdOrder::STATUS_TRANSIT          => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_TRANSIT'),
				DpdOrder::STATUS_TRANSIT_TERMINAL => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_TRANSIT'),
				DpdOrder::STATUS_ARRIVE_PICKUP    => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_READY_PICKUP'),
				DpdOrder::STATUS_ARRIVE           => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_READY'),
				DpdOrder::STATUS_COURIER          => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_COURIER'),
				DpdOrder::STATUS_DELIVERED        => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_DELIVERED'),
				DpdOrder::STATUS_NOT_DONE         => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_CANCEL'),
				DpdOrder::STATUS_PROBLEM          => Option::get(IPOLH_DPD_MODULE, 'STATUS_ORDER_PROBLEM'),
			);

			if (isset($status[$orderStatus])
				&& !empty($status[$orderStatus])
				&& $this->orderFields['STATUS_ID'] != $status[$orderStatus]
			) {
				\CSaleOrder::StatusOrder($this->orderFields['ID'], $status[$orderStatus]);
			}

			if ($this->orderFields['PAYED'] != 'Y'
				&& $orderStatus == DpdOrder::STATUS_DELIVERED
				&& Option::get(IPOLH_DPD_MODULE, 'MARK_PAYED', 0)
			) {
				\CSaleOrder::PayOrder($this->orderFields['ID'], "Y");
			}
		}

		return true;
	}

	/**
	 * Устанавливает статус заказа по его коду
	 * 
	 * @param int    $eventCode
	 * @param string $eventTime
	 * 
	 * @return self
	 */
	public function setOrderStatusByCode($eventCode, $eventTime, $eventReason = '', $eventParams = [])
	{
		$statuses = [
			1001 => DpdOrder::STATUS_OFFER_CREATE,
			1101 => DpdOrder::STATUS_OFFER_ERROR,
			1201 => DpdOrder::STATUS_OFFER_WAITING,
			1301 => DpdOrder::STATUS_OFFER_CANCEL,
			1401 => DpdOrder::STATUS_OK,
			1501 => DpdOrder::STATUS_WAITING,
			1601 => DpdOrder::STATUS_DEPARTURE,
			1701 => DpdOrder::STATUS_ARRIVED_IN_RF,
			1801 => DpdOrder::STATUS_END_CUSTOMS_CLEARANCE_IN_RF,
			1802 => DpdOrder::STATUS_TRANSIT_TERMINAL,
			2101 => DpdOrder::STATUS_TRANSIT,
			2102 => DpdOrder::STATUS_TRANSIT_RETURN,
			2201 => DpdOrder::STATUS_ARRIVE_PICKUP,
			2202 => DpdOrder::STATUS_ARRIVE_COURIER,
			2203 => DpdOrder::STATUS_ARRIVE_PICKUP_RETURN,
			2204 => DpdOrder::STATUS_ARRIVE_COURIER_RETURN,
			2301 => DpdOrder::STATUS_COURIER,
			2303 => DpdOrder::STATUS_TRANSIT_TERMINAL,
			2205 => DpdOrder::STATUS_CUSTOMS_CLEARANCE,
			2302 => DpdOrder::STATUS_END_CUSTOMS_CLEARANCE,
			2304 => DpdOrder::STATUS_COURIER,
			2305 => DpdOrder::STATUS_ARRIVE_COURIER_RETURN,
			2306 => DpdOrder::STATUS_ARRIVE_COURIER,
			2307 => DpdOrder::STATUS_PROBLEM,
			2309 => DpdOrder::STATUS_COURIER_RETURN,
			2310 => DpdOrder::STATUS_TRANSIT_SPEC,
			2401 => DpdOrder::STATUS_PROBLEM,
			2402 => DpdOrder::STATUS_PROBLEM,
			2404 => DpdOrder::STATUS_DELIVERY_PROBLEM,
			2405 => DpdOrder::STATUS_DELIVERY_PROBLEM,
			2406 => DpdOrder::STATUS_DELIVERY_PROBLEM,
			2407 => DpdOrder::STATUS_PROBLEM,
			2408 => DpdOrder::STATUS_PROBLEM,
			2409 => DpdOrder::STATUS_PROBLEM,
			2410 => DpdOrder::STATUS_PROBLEM,
			3701 => DpdOrder::STATUS_PROBLEM,
			2501 => DpdOrder::STATUS_NOT_DONE,
			2901 => DpdOrder::STATUS_CANCEL,
			3301 => DpdOrder::STATUS_REMOVED,
			3302 => DpdOrder::STATUS_NOT_CLAIMED,
			3303 => DpdOrder::STATUS_LOST,
			3304 => DpdOrder::STATUS_DELIVERED,
			3305 => DpdOrder::STATUS_DELIVERED,
			3306 => DpdOrder::STATUS_RETURNED,
		];

		if (!array_key_exists($eventCode, $statuses)) {
			return false;
		}

		$status = $statuses[$eventCode];
		$message = isset($eventParams['ERROR_MESSAGE'])
			? $eventParams['ERROR_MESSAGE']
			: $eventReason
		;

		if (array_key_exists('OrderWorkCompleted', $eventParams)) {
			$unitLoads = $this->unitLoads;
			
			foreach ($eventParams['OrderWorkCompleted'] as $item) {
				foreach ($unitLoads as &$unitLoad) {
					if ($unitLoad['NAME'] != $item['DESCRIPT']
						|| $unitLoad['QUANTITY'] != $item['COUNT']
					) {
						continue;
					}

					$unitLoad['RETURN']      = $item['STATE_NAME'] == Loc::getMessage('IPOLH_DPD_RETURN_STATE_NAME') ? 'Y' : 'N';
					$unitLoad['RETURN_DESC'] = $item['REJECT_REASON_NAME'] ?: '';

					break;
				}
			}

			$this->unitLoads = $unitLoads;
		}

		$ret = $this->setOrderStatus($status, $eventTime, $message);

		

		if ($ret) {
			


		}

		return $ret;
	}

	/**
	 * Выставляет вариант оплаты доставки
	 *
	 * @return void
	 */
	public function setPaymentType($value)
	{
		$this->fields['PAYMENT_TYPE'] = $value;
		$this->reloadUnits();
	}
	
	/**
	 * Выставляет флаг НПП
	 *
	 * @return void
	 */
	public function setNpp($value)
	{
		$this->fields['NPP'] = $value == 'Y' ? 'Y' : 'N';
		$this->reloadUnits();
	}

	public function setChst($value)
	{
		$this->fields['CHST'] = $this->isSelfDelivery() ? '' : $value;
		$this->reloadUnits();
 	}

	/**
	 * Возвращает сумму наложенного платежа
	 *
	 * @return float
	 */
	public function getSumNpp()
	{
		$ret = 0;

		foreach ($this->fields['UNIT_LOADS'] as $item) {
			$ret += $item['QUANTITY'] * $item['NPP'];
		}

		return $ret;
	}

	/**
	 * Устанавливает сумму наложенного платежа
	 * 
	 * @deprecated use setUnitLoads method
	 */
	public function setSumNpp($sum)
	{
		throw new SystemException('use setUnitLoads method for set npp sum');
	}

	/**
	 * Выставляет флаг ОЦ
	 * 
	 * @return void
	 */
	public function setUseCargoValue($value)
	{
		$this->fields['USE_CARGO_VALUE'] = $value == 'Y' ? 'Y' : 'N';
		$this->reloadUnits();
	}

	/**
	 * Возвращает сумму наложенного платежа
	 *
	 * @return float
	 */
	public function getCargoValue()
	{
		$ret = 0;

		foreach ((array) $this->fields['UNIT_LOADS'] as $item) {
			$ret += $item['QUANTITY'] * $item['CARGO'];
		}

		return $ret;
	}

	/**
	 * Устанавливает сумму ОЦ
	 * 
	 * @deprecated use setUnitLoads method
	 */
	public function setCargoValue($sum)
	{
		throw new SystemException('use setUnitLoads method for set cargo value');
	}

	/**
	 * Устанавливает состав заказа
	 * На основе состава заказа расчитываются данные ОЦ и НПП
	 *
	 * @param array $value
	 * 
	 * @return void
	 */
	public function setUnitLoads($value)
	{	
		$items = [];
		$isReplaced = true;

		foreach ($value as $k => $item) {
			$item['QUANTITY'] = (int) $item['QUANTITY'];
			
			if (isset($item['CARGO'])) {
				if ($this->fields['USE_CARGO_VALUE'] != 'Y') {
					$item['CARGO'] = 0;
				} else {
					$item['CARGO'] = round($item['CARGO'], 2);
				}
			}
			
			if (isset($item['NPP'])) {
				if ($this->fields['NPP'] != 'Y') {
					$item['NPP'] = 0;
				} else {
					$item['NPP'] = round($item['NPP'], 2);
				}
			}

			if ($item['ID'] == 'DELIVERY') {

				if ($this->paymentType != 'OUP' && ($this->chst == '' || $this->chst == 'N')) {
					$items[$k] = $item;
				}
			} else {
				$items[$k]  = $item;
			}

			if (!isset($item['ID'])) {
				$isReplaced = false;
			}
		}

		if ($isReplaced) {
			$this->fields['UNIT_LOADS'] = $items;
		} else {
			foreach ($items as $id => $item) {
				if (in_array($id, array_column($this->fields['UNIT_LOADS'], 'ID'))) {
					foreach ($this->fields['UNIT_LOADS'] as &$curItem) {
						if ((string) $curItem['ID'] !== (string) $id) {
							continue;
						}
						
						$curItem = array_merge($curItem, $item);
					}
				} elseif (isset($item['ID'])) {
					$this->fields['UNIT_LOADS'][] = $item;
				}
			}
		}
	}

	/**
	 * Перезаполняет UNIT_LOADS на основе данных о заказе
	 * 
	 * @return void
	 */
	public function reloadUnits()
	{
		if (empty($this->fields['ORDER_ID'])) {
			return ;
		}


		$this->unitLoads  = array_merge(array_map(function($item) {
			$product = \CCatalogProduct::GetByID($item['PRODUCT_ID']);
			$vatRate = '';

			if ($product['VAT_ID']) {
				$vat     = \CCatalogVat::GetByID($product['VAT_ID'])->Fetch();
				$vatRate = $vat && $vat['NAME'] != Loc::getMessage('IPOLH_DPD_ORDER_WITHOUT_VAT') ? $vat['RATE'] : '';
			}

			return [
				'ID'       => $item['PRODUCT_ID'],
				'NAME'     => $item['NAME'],
				'QUANTITY' => $item['QUANTITY'],
				'CARGO'    => $item['PRICE'],
				'NPP'      => $item['PRICE'],
				'VAT'      => $vatRate,
			];
		}, $this->orderItems), [
			[
				'ID'       => 'DELIVERY',
				'NAME'     => Loc::getMessage('IPOLH_DPD_DELIVERY_ITEM_NAME'),
				'QUANTITY' => 1,
				'CARGO'    => 0,
				'NPP'      => $this->priceDelivery,
				'VAT'      => $this->getDeliveryVat(),
			]
		]);
	}

	public function setSender($index)
	{
		$senders = \Bitrix\Main\Config\Option::get(IPOLH_DPD_MODULE, 'SENDERS', 'a:0:{}');
		$senders = unserialize($senders) ?: [];

		if (isset($senders[$index])) {
			$fields = [
				'FIO', 'NAME', 'PHONE', 'EMAIL',
				/* 'REGULAR_NUM' ,*/ 'NEED_PASS',
				'LOCATION', 'STREET',  'STREETABBR', 'HOUSE',
				'KORPUS', 'STR', 'VLAD', 'OFFICE', 'TERMINAL_CODE'
			];

			foreach ($fields as $field) {
				$this->fields['SENDER_'. $field] = $senders[$index][$field] ?: $this->fields['SENDER_'. $field];
			}

			$this->shipment = null;

		} else {
			$index = 'OTHER';
		}

		$this->fields['SENDER'] = $index;

		return $this;
	}

	// public function setSenderLocation($value)
	// {
	// 	if (empty($this['SENDER']) || $this['SENDER'] == 'OTHER') {
	// 		$this->fields['SENDER_LOCATION'] = $value;
	// 		$this->shipment = null;
	// 	}
		
	// 	return $this;
	// }

	public function offsetGet($prop)
	{
		if ($prop == 'SUM_NPP' || $prop == 'CARGO_VALUE')  {
			return $this->__get($prop);
		}
		
		return parent::offsetGet($prop);
	}

	/**
	 * @return boolean
	 */
	public function isNew()
	{
		return $this->fields['ORDER_STATUS'] == DpdOrder::STATUS_NEW;
	}

	/**
	 * Проверяет отправлялся ли заказ в DPD
	 *
	 * @return boolean
	 */
	public function isCreated()
	{
		return $this->fields['ORDER_STATUS'] != DpdOrder::STATUS_NEW
			&& $this->fields['ORDER_STATUS'] != DpdOrder::STATUS_CANCEL;
	}

	/**
	 * Проверяет отправлялся ли заказ в DPD и был ли он там успешно создан
	 *
	 * @return boolean
	 */
	public function isDpdCreated()
	{
		return $this->isCreated() && !empty($this->fields['ORDER_NUM']);
	}

	/**
	 * Возвращает инстанс для работы с внешним заказом
	 *
	 * @return \Ipolh\DPD\Order;
	 */
	public function dpd()
	{
		$locationTo = $this->getShipment()->getReceiver();
		$locationCountryCode = false;

		if (\Ipolh\DPD\API\User::isActiveAccount($locationTo['COUNTRY_CODE'])) {
			$locationCountryCode = $locationTo['COUNTRY_CODE'];
		}

		return new DpdOrder($this, \Ipolh\DPD\API\User::getInstance($locationCountryCode));
	}

	/**
	 * Загружает информацию о заказе
	 *
	 * @param  int $orderId ID заказа
	 * @return void
	 */
	protected function loadOrder()
	{
		$this->loadOrderFields();
		$this->loadOrderProps();
		$this->loadOrderItems();
	}

	/**
	 * Загружает поля заказа
	 *
	 * @param  int $orderId ID заказа
	 * @return void
	 */
	protected function loadOrderFields()
	{
		$this->orderFields  = \CSaleOrder::GetList(
			$arOrder  = [],
			$arFilter = [
				Option::get(IPOLH_DPD_MODULE, 'ORDER_ID', 'ID') => $this->ORDER_ID,
			]
		)->Fetch();
	}

	/**
	 * Загружает значения св-в заказа
	 *
	 * @param  int $orderId ID заказа
	 * @return void
	 */
	protected function loadOrderProps()
	{
		$arPropValues = array();
		$rsPropValues = \CSaleOrderPropsValue::GetOrderProps($this->orderFields['ID']);
		while($arPropValue = $rsPropValues->Fetch()) {
			$code = $arPropValue['CODE'] ?: 'PROP_'. $arPropValue['ORDER_PROPS_ID'];
			$arPropValues[$code] = $arPropValue['VALUE'];
		}

		$this->orderProps = array();
		$arProps = \Ipolh\DPD\Utils::GetOrderProps($this->orderFields['PERSON_TYPE_ID']);
		foreach ($arProps as $arProp) {
			$code  = $arProp['CODE'] ?: 'PROP_'. $arProp['ID'];
			$this->orderProps[$code] = $arPropValues[$code];
		}
	}

	/**
	 * Загружает информацию о составе заказа
	 * @return void
	 */
	protected function loadOrderItems()
	{
		$this->orderItems = array();

		$rsItems = \CSaleBasket::GetList(
			$arOrder  = array(),
			$arFilter = array(
				'ORDER_ID' => $this->orderFields['ID'],
			),
			$arGroupBy = false,
			$arNavParms = false,
			$arSelect = array(
				"ID", "PRODUCT_ID", "SET_PARENT_ID", "PRICE", "QUANTITY", "CAN_BUY",
				"DELAY", "NAME", "DIMENSIONS", "WEIGHT", "PRICE",
				"SET_PARENT_ID", "LID",
			)
		);

		$items = [];
		while ($arItem = $rsItems->Fetch()) {
			if (!($arItem['CAN_BUY'] == 'Y' && $arItem['DELAY'] == 'N')) {
				continue;
			}

			$items[$arItem['PRODUCT_ID']] = $arItem;
		}

		// выделяем из заказа комлекты
		$complects = array();
		foreach ($items as $k => $item) {
			if (isset($item['SET_PARENT_ID'])
				&& $item['SET_PARENT_ID'] > 0
				&& $item['SET_PARENT_ID'] != $item['ID']
			) {
				$complects[] = $item['SET_PARENT_ID'];
			}
		}

		// оставляем в составе заказа только сами товары, удаляя комплекты
		$this->orderItems = array_filter($items, function($item) use ($complects) { return !in_array($item['ID'], $complects); });
	}

	/**
	 * Возвращает НДС для доставки
	 *
	 * @return mixed
	 */
	protected function getDeliveryVat()
	{
		$ret = '';

		if (empty($this->fields['ORDER_ID'])) {
			return $ret;
		}

		$arOrder = \CSaleOrder::GetList(
			$arOrder  = [],
			$arFilter = [
				Option::get(IPOLH_DPD_MODULE, 'ORDER_ID', 'ID') => $this->ORDER_ID,
			]
		)->Fetch();
		
		$order = \Bitrix\Sale\Order::load($arOrder['ID']);

		foreach ($order->getShipmentCollection() as $shipment) {
			if ($shipment->isSystem()) {
				continue;
			}

			$delivery = $shipment->getDelivery();
			$vatId = $delivery->getVatId();

			if ($vatId) {
				$vat = \CCatalogVat::GetByID($vatId)->Fetch();

				if ($vat && $vat['NAME'] != Loc::getMessage('IPOLH_DPD_ORDER_WITHOUT_VAT')) {
					$ret = $vat['RATE'];
					break;
				}
			}
		}

		return $ret;
	}

	public function setPickupTimePeriod($value) {
		if ($this->serviceCode == 'DAY') {
			$value = '14-15';
		}

		$this->fields['PICKUP_TIME_PERIOD'] = $value;

		return $this;
	}

	public function setDeliveryTimePeriod($value) {
		if ($this->serviceCode == 'DAY') {
			$value = '19-23';
		}

		$this->fields['DELIVERY_TIME_PERIOD'] = $value;

		return $this;
	}

	public function setPickupDate($value)
	{
		if ($this->serviceCode == 'DAY') {
			$value = date('d.m.Y');
		}

		$this->fields['PICKUP_DATE'] = $value;

		return $this;
	}

	public function setCargoNumPack($value)
	{
		if ($this->serviceCode == 'DAY') {
			$value = 1;
		}

		$this->fields['CARGO_NUM_PACK'] = $value;

		return $this;
	}

	public function setOgd($value)
	{
		if ($this->serviceCode == 'DAY') {
			$value = '';
		}

		$this->fields['OGD'] = $value;

		return $this;
	}
}