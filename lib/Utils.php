<?php
namespace Ipolh\DPD;

class Utils
{
	/**
	 * Переводит строку из under_score в camelCase
	 * 
	 * @param  string  $string                   строка для преобразования
	 * @param  boolean $capitalizeFirstCharacter первый символ строчный или прописной
	 * @return string
	 */
	public static function underScoreToCamelCase($string, $capitalizeFirstCharacter = false)
	{
		// символы разного регистра
		if (/*strtolower($string) != $string
			&&*/ strtoupper($string) != $string
		) {
			return $string;
		}

		$string = strtolower($string);
		$string = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

		if (!$capitalizeFirstCharacter) {
			$string[0] = strtolower($string[0]);
		}

		return $string;
	}

	/**
	 * Переводит строку из camelCase в under_score
	 * 
	 * @param  string  $string    строка для преобразования
	 * @param  boolean $uppercase
	 * @return string
	 */
	public static function camelCaseToUnderScore($string, $uppercase = true)
	{
		// символы разного регистра
		if (strtolower($string) != $string
			&& strtoupper($string) != $string
		) {
			$string = ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $string)), '_');;
		}		

		if ($uppercase) {
			$string = strtoupper($string);
		}

		return $string;
	}

	/**
	 * Конверирует кодировку
	 * В качестве значений может быть как скалярный тип, так и массив
	 *
	 * @param mixed $data
	 * @param string $fromEncoding
	 * @param string $toEncoding
	 */
	public static function convertEncoding($data, $fromEncoding, $toEncoding)
	{
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				$data[$key] = static::convertEncoding($value, $fromEncoding, $toEncoding);
			}
		} else {
			$data = iconv($fromEncoding, $toEncoding, $data);
		}

		return $data;
	}

	/**
	 * Возвращает свойства заказа для типа платильщика
	 * 
	 * @param  int $personeTypeId
	 * @return array
	 */
	public static function getOrderProps($personeTypeId)
	{
		$rsProps = \CSaleOrderProps::GetList(
			$arOrder  = array('SORT' => 'ASC'),
			$arFilter = array('PERSON_TYPE_ID' => $personeTypeId)
		);

		$result = array();
		while($arProp = $rsProps->Fetch()) {
			$result[] = $arProp;
		}

		return $result;
	}

	/**
	 * Возвращает значение св-в заказа
	 * 
	 * @param  int $orderId
	 * @param  int $personeTypeId
	 * @return array
	 */
	public static function getOrderPropsValue($orderId, $personeTypeId = false)
	{}

	/**
	 * Конвертирует дату из bitrix формата в DPD
	 * 
	 * @param string $date
	 * @return string
	 */
	public static function DateBitrixToDpd($date)
	{
		return ConvertDateTime($date, "YYYY-MM-DD", "ru");
	}
	
	/**
	 * Конвертирует дату из DPD формата в bitrix
	 * 
	 * @param string $date
	 * @return string
	 */
	public static function DateDpdToBitrix($date)
	{
		return ConvertDateTime($date, "DD.MM.YYYY", "ru");
	}

	/**
	 * Возвращает список отправителей
	 *
	 * @return array
	 */
	public static function getSenders()
	{
		$items = \Bitrix\Main\Config\Option::get(IPOLH_DPD_MODULE, 'SENDERS', 'a:0:{}');

		return unserialize($items) ?: [];
	}

	/**
	 * Возвращает индекс отправителя
	 *
	 * @return int
	 */
	public static function getSenderIndexDefault()
	{
		foreach (static::getSenders() as $index => $sender) {
			if ($sender['DEFAULT'] == 'Y') {
				return $index;
			}
		}

		return 'OTHER';
	}

	public static function getSenderIndexByLocation($locationId)
	{
		$groups = static::getGroupsByLocation($locationId);

		foreach (static::getSenders() as $index => $sender) {
			$groupIds = array_filter((array) $sender['LOCATION_GROUPS']);

			if (array_intersect($groups, $groupIds)) {
				return $index;
			}
		}

		return false;
	}

	public static function getGroupsByLocation($locationId)
	{
		$locationId = static::getLicationId($locationId);

		$location = \Bitrix\Sale\Location\LocationTable::getList([
			'filter' => ['=ID' => $locationId],
			'select' => ['ID', 'LEFT_MARGIN', 'RIGHT_MARGIN']
		])->fetch();

		if (!$location) {
			return [];
		}

		$locations = array_merge(
			[$locationId],
			
			array_column(\Bitrix\Sale\Location\LocationTable::getList([
				'filter' => [
					'<LEFT_MARGIN'    => $location['LEFT_MARGIN'],
					'>RIGHT_MARGIN'    => $location['RIGHT_MARGIN'],
					// 'NAME.LANGUAGE_ID' => LANGUAGE_ID,
				],
				'select' => ['ID']
			])->fetchAll(), 'ID')
		);

		$items = \Bitrix\Sale\Location\GroupLocationTable::getList([
			'filter' => ['=LOCATION_ID' => $locations]
		])->fetchAll();

		return array_column($items, 'LOCATION_GROUP_ID');
	}

	/**
	 * Возвращает ID местоположение по его коду
	 *
	 * @param $idOrCode
	 * 
	 * @return int
	 */
	public static function getLicationId($idOrCode)
	{
		$arBxLocation = \Bitrix\Sale\Location\LocationTable::getList([
			'filter' => [
				'=CODE' => $idOrCode
			],
		
			'select' => ['ID', 'CITY_ID', 'SORT', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'CODE']
		])->fetch();
		
		if(!is_array($arBxLocation)) {
			$arBxLocation = \Bitrix\Sale\Location\LocationTable::getList([
				'filter' => [
					'=ID' => $idOrCode
				],
		
				'select' => ['ID', 'CITY_ID', 'SORT', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'CODE']
			])->fetch();
		}

		if (!$arBxLocation) {
			return false;
		}

		return $arBxLocation['ID'];
	}

	/**
	 * Возвращает местоположение магазина
	 * 
	 * @return int
	 */
	public static function getSaleLocationId()
	{
		$defaultLocation = \Bitrix\Main\Config\Option::get('sale', 'location', '', defined('ADMIN_SECTION') && ADMIN_SECTION ? 's1' : false);
		$currentLocation = \Bitrix\Main\Config\Option::get(IPOLH_DPD_MODULE, 'SENDER_LOCATION', $defaultLocation);

		$senders = static::getSenders();

		foreach ($senders as $sender) {
			if ($sender['DEFAULT'] == 'Y') {
				$currentLocation = $sender['LOCATION'];
				
				break;
			}
		}

		$arBxLocation = \CSaleLocation::GetByID($currentLocation, "ru");

		if (!$arBxLocation) {
			return false;
		}

		return $arBxLocation['ID'];
	}

	public static function isNeedBreak($start_time)
	{
		$max_time = (ini_get('max_execution_time') ?: 60);
		$max_time = max(min($max_time, 60), 10);

		$max_time = 20;

		return time() >= ($start_time + $max_time - 5);
	}

	/**
	 * Возвращает идентификатор или номер заказа в зависимости от настроек модуля
	 * 
	 * @param  array $order
	 * 
	 * @return string
	 */
	public static function getOrderId($order)
	{
		$key = \Bitrix\Main\Config\Option::get(IPOLH_DPD_MODULE, 'ORDER_ID', 'ID');

		return $order[$key ?: 'ID'];
	}
}