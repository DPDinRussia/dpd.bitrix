<?php
namespace Ipolh\DPD\API\Client;

use \Bitrix\Main\SystemException;
use \Bitrix\Main\Data\Cache;

use \Ipolh\DPD\API\User;
use \Ipolh\DPD\Utils;
use \Ipolh\DPD\Debug\Log;

class Soap extends \Dklab_SoapClient implements ClientInterface
{
	public $convertEncoding = true;

    /**
     * Параметр default_socket_timeout
     * @var integer
     */
    protected $socketTimeout = 600;

	/**
	 * адрес wsdl схемы
	 * @var string
	 */
	protected $wsdl = '';

	/**
	 * Параметры авторизации
	 * @var array
	 */
	protected $auth = array();

	/**
	 * @var
	 */
	protected $cache;

	/**
	 * Время хранения кеша
	 * @var integer
	 */
	protected $cache_time = IPOLH_DPD_CACHE_TIME;

	/**
	 * Параметры для SoapClient
	 * @var array
	 */
	protected $soap_options = array('connection_timeout' => 10);

	/**
	 * @var boolean
	 */
	protected $strictMode = false;

	/**
	 * @var boolean
	 */
	protected $initError = false;

	/**
	 * Конструктор класса
	 * 
	 * @param string $wsdl
	 * @param User   $user
	 * @param array  $options
	 */
	public function __construct($wsdl, User $user, array $options = array())
	{
		try {
			ini_set('default_socket_timeout', $this->socketTimeout);

			$this->wsdl = $user->resolveWsdl($wsdl);
			$this->auth = array(
				'clientNumber' => $user->getClientNumber(),
				'clientKey'    => $user->getSecretKey(),
			);

			$this->options = $options;

			if (empty($this->auth['clientNumber']) || empty($this->auth['clientKey'])) {
				throw new SystemException('DPD: Authentication data is not provided');
			}

			if(!$this->checkServerAvailability($this->wsdl)) {
                throw new SystemException('DPD: Server Unavailable');
            }

			parent::__construct($this->wsdl,
                array_merge(
                    $this->soap_options,
                    $options,
                    ['exceptions' => true]
                )
            );

		} catch (\Exception $e) {
			$this->initError = $e->getMessage();
		}
	}

	/**
	 * Устанавливает время жизни кэша
	 * @param int $cacheTime
	 */
	public function setCacheTime($cacheTime)
	{
		$this->cache_time = $cacheTime;
	}

	/**
	 * Выполняет запрос к внешнему API
	 *
	 * TODO: в качестве возвращаемого результата использовать \Bitrix\Main\Result
	 * 
	 * @param  string $method
	 * @param  array  $args
	 * @param  string $wrap
	 * @return mixed
	 */
	public function invoke($method, array $args = array(), $wrap = 'request', $keys = false)
	{
		try {
			if ($this->initError) {
				throw new SystemException($this->initError);
			}

			$async = (bool) $args['async'];
			unset($args['async']);

			$parms   = array_merge($args, array('auth' => $this->auth));
			$request = $wrap ? array($wrap => $parms) : $parms;
			$request = $this->convertDataForService($request);

			$cache_id = serialize($request) . ($keys ? serialize($keys) : '') . '2';
			$cache_path = '/'. IPOLH_DPD_MODULE .'/api/'. $method;

			if ($this->cache_time > 0 && $this->cache()->initCache($this->cache_time, $cache_id, $cache_path)) {
				return $this->cache()->GetVars();
			}

			if ($async) {
				return $this->async->$method($request);
			}

			$ret = $this->$method($request);

			// hack return binary data
			if ($ret 
				&& isset($ret->return->file)
			) {
				return array('FILE' => $ret->return->file);
			}

			$ret = json_encode($ret);
			$ret = json_decode($ret, true);
			$ret = $ret['return'];

			if ($keys && is_array($ret) && array_intersect((array) $keys, array_keys($ret))) {
				$ret = [$ret];
			}

			$ret = $this->convertDataFromService($ret, $keys);

			if ($this->cache()->startDataCache()) {
				$this->cache()->endDataCache($ret);
			}

			return $ret;

		} catch (\Exception $e) {
			
			\AddMessage2Log(
				sprintf('Error invoke method %s in %s: %s', $method, $this->wsdl, $e->getMessage()),
				'ipol.dpd',
				6, true
			);

			if ($this->options['exceptions']) {
				throw $e;
			}
		}

		return false;
	}

	/**
	 * Возвращает инстанс кэша
	 * 
	 * @return \Bitrix\Main\Data\Cache
	 */
	protected function cache()
	{
		return $this->cache ?: $this->cache = Cache::createInstance();
	}

	/**
	 * Конвертирует переданные данные в формат внешнего API
	 *
	 * Под конвертацией понимается:
	 * - перевод названий параметров в camelCase
	 * - смена кодировки при необходимости
	 * 
	 * @param  array $data 
	 * @return array
	 */
	protected function convertDataForService($data)
	{
		$ret = array();
		foreach ($data as $key => $value) {
			if ($key != 'GTIN') {
				$key = Utils::underScoreToCamelCase($key);
			}

			$ret[$key] = is_array($value) 
							? $this->convertDataForService($value)
							: ($this->convertEncoding ? Utils::convertEncoding($value, SITE_CHARSET, 'UTF-8') : $value);
		}

		return $ret;
	}

	protected function convertDataFromService($data, $keys = false)
	{
		$keys = $keys ? array_flip((array) $keys) : false;

		$ret = array();
		foreach ($data as $key => $value) {
			$key = $keys 
					? implode(':', array_intersect_key($value, $keys))
					: Utils::camelCaseToUnderScore($key);

			$ret[$key] = is_array($value)
							? $this->convertDataFromService($value)
							: ($this->convertEncoding ? Utils::convertEncoding($value, 'UTF-8', SITE_CHARSET) : $value);
		}

		return $ret;
	}

    /**
     * Проверяет доступен ли сервер по указанному URL
     *
     * @param  string $location
     * @return boolean
     */
    protected function checkServerAvailability($url)
    {
        $locationArr = parse_url($url);

        /*
         * Проверка с использованием curl
         * */
        if (extension_loaded('curl')
            && !empty($locationArr['host'])) {

            $locationLink = $locationArr['scheme'].'://' . $locationArr['host'] . $locationArr['path'];
            if( !empty($locationArr['query']) ) {
                $locationLink .= '?' . $locationArr['query'];
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $locationLink);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->socketTimeout);

            curl_exec($ch);
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($statusCode > 0 && $statusCode < 400) {
                return true;
            } else {
                return false;
            }
        }

        /*
         * Проверка с использованием file_get_contents
         * */
        if (function_exists('file_get_contents')
            && !empty($locationArr['host'])) {

            $urlContent = file_get_contents(
                $url,
                false,
                stream_context_create(["http" => [
                    "timeout" => $this->socketTimeout,
                    "ssl" => [
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                    ],
                ]])
            );
            if( !empty($urlContent) ) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }
}