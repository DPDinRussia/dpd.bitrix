<?php
namespace Ipolh\DPD\API\Client;

use \Ipolh\DPD\API\User;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Factory
{
	public static function create($wdsl, User $user, $strictMode = false)
	{
		if (class_exists('\\SoapClient')) {
			return new Soap($wdsl, $user, [
				'exceptions' => $strictMode
			]);
		}

		throw new \Exception(Loc::getMessage('IPOLH_DPD_ERROR_SOAP_NOT_INSTALLED'));
	}
}