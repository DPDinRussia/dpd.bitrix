<?php
namespace Ipolh\DPD\Admin\Options;

use \Bitrix\Main\Result;
use \Bitrix\Main\Error;
use \Bitrix\Main\Config\Option;
use \Ipolh\DPD\Admin\Form\AbstractForm;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/'. IPOLH_DPD_MODULE .'/options.php');

class Sender extends AbstractForm
{
    protected $formName = 'IPOLH_DPD_OPTIONS_SENDER';

    public function __construct($index = null)
    {
        $this->index = $index;
        $this->loadItem();
    }
    
    /**
	 * Отрисовывает форму
	 *
	 * @param  \Bitrix\Main\Result  $result
	 * @return string
	 */
	public function render(\Bitrix\Main\Result $result = null)
	{
        \CJSCore::Init('ipolh_dpd_admin_options_sender');

        $this->actionUrl = $GLOBALS['APPLICATION']->GetCurPageParam("index=". $this->index, ['index']);

		return ''
			. '<div id="IPOLH_DPD_OPTIONS_SENDER_FORM" style="margin: -12px">'
			. '		<style>#IPOLH_DPD_OPTIONS_SENDER_FORM .adm-detail-content {padding: 12px;}</style>'
			. '		<style>#IPOLH_DPD_OPTIONS_SENDER_FORM .adm-detail-content-btns-wrap {display: none;}</style>'
			. 		parent::render($result)
			. '</div>'
		;
	}

	/**
	 * Поля формы
	 *
	 * @return array
	 */
	public function getFields()
	{
		return [
			[
				'DIV'      => 'IPOLH_DPD_SERVICES_TAB_SENDER',
				'TAB'      => Loc::getMessage('IPOLH_DPD_SERVICES_TAB_SENDER'),
				'ICON'     => '',
				'TITLE'    => Loc::getMessage('IPOLH_DPD_SERVICES_TAB_SENDER_TITLE'),
				'HELP'     => '',
				'OPTIONS'  => [],
				'CONTROLS' => [
                    'DEFAULT' => array(
                        'TITLE'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_DEFAULT"),
                        'HELP'   => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_DEFAULT_HELP"),
                        'TYPE'   => 'checkbox',
                        'VALUE'  => 'Y',
                    ),

                    'NAME' => array(
                        'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME"),
                        'TYPE'    => 'string',
                    ),

					'SENDER_FIO' => array(
						'TYPE'  => 'STRING',
						'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FIO"),
						'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FIO_HELP"),
						"VALIDATORS" => array(
							"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FIO_REQUIRED"),
						),
					),
		
					'SENDER_NAME' => array(
						'TYPE'  => 'STRING',
						'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME"),
						'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME_HELP"),
						"VALIDATORS" => array(
							"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NAME_REQUIRED"),
						),
					),
		
					'SENDER_PHONE' => array(
						'TYPE'  => 'STRING',
						'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_PHONE"),
						"VALIDATORS" => array(
							"required" => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_PHONE_REQUIRED"),
						),
					),
		
					'SENDER_EMAIL' => array(
						'TYPE'  => 'STRING',
						'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_EMAIL"),
						"VALIDATORS" => array(
							function($field, $value, $form) {
								$values = $form->getEditItem();
		
								if ($values['SENDER_NEED_PASS'] == 'Y' && empty($value)) {
									return Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_EMAIL_REQUIRED");
								}
							}
						),
					),
		
					'SENDER_REGULAR_NUM' => array(
						'TYPE'  => 'STRING',
						'TITLE' => Loc::getMessage("IPOLG_DPD_OPTIONS_SENDER_REGULAR_NUM"),
					),
		
					'SENDER_NEED_PASS' => array(
						'TYPE'          => 'CHECKBOX',
						'TITLE'         => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_NEED_PASS"),
						'VALUE'         => 'Y',
						'UNCHECK_VALUE' => 'N',
					),
		
					'SENDER_SUBHEADER_ADDRESS' => array(
						'TYPE'  => 'HEADER',
						'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_ADDRESS_SUBHEADER"),
					),

                    'LOCATION' => array(
                    	'TITLE'   => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_LOCATION"),
                    	'TYPE'    => 'location',
                    	'DEFAULT' => \Ipolh\DPD\Utils::getSaleLocationId(),
					),
					
					'LOCATION_GROUPS' => array(
						'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_LOCATION_GROUPS"),
						'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_LOCATION_GROUPS_HELP"),
						'TYPE'  => 'select',
						'ITEMS' => function() {

							$ret = ['' => ''];

							$items = \Bitrix\Sale\Location\GroupTable::getList([
								'filter' => array('NAME.LANGUAGE_ID' => LANGUAGE_ID),
								'select' => array(
									'ID',
									'GROUP_NAME' => 'NAME.NAME'
								)
							]);

							foreach ($items as $item) {
								$ret[ $item['ID'] ] = $item['GROUP_NAME'];
							}

							return $ret;
						},
						"MULTIPLE" => "Y",
					),

                    'SENDER_TABS' => array(
                    	'TYPE'  => 'TABS',
                    	'ITEMS' => array(
                    		array(
                    			'DIV'      => "IPOLH_DPD_OPTIONS_SENDER_SUBTAB_COURIER",
                    			'TAB'      => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_SUBTAB_COURIER"),
                    			'ICON'     => '',
                    			'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_SUBTAB_COURIER_TITLE"),
                    			'OPTIONS'  => array(),
                    			'CONTROLS' => array(
                    				'STREET' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_STREET"),
                    				),

                    				'STREETABBR' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_STREETABBR"),
                    					'HELP'  => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_STREETABBR_HELP"),
                    				),

                    				'HOUSE' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_HOUSE"),
                    				),

                    				'KORPUS' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_KORPUS"),
                    				),

                    				'STR' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_STR"),
                    				),

                    				'VLAD' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_VLAD"),
                    				),

                    				'OFFICE' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_OFFICE"),
                    				),

                    				'FLAT' => array(
                    					'TYPE'  => 'STRING',
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_FLAT"),
                    				),
                    			),
                    		),

                    		array(
                    			'DIV'      => "IPOLH_DPD_OPTIONS_SENDER_SUBTAB_PICKUP",
                    			'TAB'      => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_SUBTAB_PICKUP"),
                    			'ICON'     => '',
                    			'TITLE'    => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_SUBTAB_PICKUP_HELP"),
                    			'OPTIONS'  => array(),
                    			'CONTROLS' => array(
                    				'TERMINAL_CODE' => array(
                    					'TITLE' => Loc::getMessage("IPOLH_DPD_OPTIONS_SENDER_TERMINAL_CODE"),
                    					'TYPE'  => 'SELECT',
                    					'ITEMS' => function() {
                    						$ret = \Ipolh\DPD\DB\Terminal\Table::getList([
                    							'select' => [
                    								'CODE',
                    								'NAME',
                    							],

                    							'filter' => [
                    								'LOCATION_ID' => $this->editItem['LOCATION'],
                    								'!SCHEDULE_SELF_PICKUP' => false,
                    							],

                    							'order' => ['NAME' => 'ASC']
                    						]);

                    						$ret->addReplacedAliases(['CODE' => 'ID']);

											return array_merge(
												['' => ''],
												$ret->fetchAll()
											);
                    					},
                    				),
                    			),
                    		),
                    	),
                    ),
				]
			],
		];
	}
	

	/**
	 * Загружает редактируемую запись из БД
	 * 
	 * @return void
	 */
	protected function loadItem()
	{
		$defaults = [
            'DEFAULT'             => 'N',
            'NAME'                => '',
			'SENDER_FIO'          => '',
            'SENDER_NAME'         => '',
            'SENDER_PHONE'        => '',
            'SENDER_EMAIL'        => '',
            'SENDER_REGULAR_NUM'  => '',
            'SENDER_NEED_PASS'    => '',
			'LOCATION'            => 0,
			'LOCATION_GROUPS'     => [],
            'STREET'              => '',
            'STREETABBR'          => '',
            'HOUSE'               => '',
            'KORPUS'              => '',
            'STR'                 => '',
            'VLAD'                => '',
            'OFFICE'              => '',
            'FLAT'                => '',
            'TERMINAL_CODE'       => '',
        ];

        $data = Option::get(IPOLH_DPD_MODULE, 'SENDERS');
        $data = unserialize($data) ?: [];
		$data = array_key_exists($this->index, $data) ? $data[$this->index] : [];
		$data = array_merge($defaults, $data);

        $this->editItem = new \ArrayObject($data);
	}

	/**
	 * Сохраняет редактируемую запись в БД
	 * 
	 * @return void
	 */
	protected function saveItem()
	{
        $data = Option::get(IPOLH_DPD_MODULE, 'SENDERS');
		$data = unserialize($data) ?: [];

        if ($this->editItem['DEFAULT'] == 'Y') {
            foreach ($data as $k => $v) {
                $data[$k]['DEFAULT'] = 'N';
            }
        }

        if (array_key_exists($this->index, $data)) {
            $data[$this->index] = (array) $this->editItem;
        } else {
            $data[] = (array) $this->editItem;
        }

		$this->index = end(array_keys($data));
		
		$data = array_filter($data, function($item) {
			unset($item['DEFAULT']);

			$item = array_filter($item);

			return sizeof($item) > 0;
		});

        Option::set(IPOLH_DPD_MODULE, 'SENDERS', serialize($data));
	}
}