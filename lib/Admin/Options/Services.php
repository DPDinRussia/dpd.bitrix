<?php
namespace Ipolh\DPD\Admin\Options;

use \Bitrix\Main\Result;
use \Bitrix\Main\Localization\Loc;
use \Ipolh\DPD\Admin\Form\AbstractForm;

class Services extends AbstractForm
{
	protected $formName = 'IPOLH_DPD_SERVICES';

	/**
	 * Конструктор класса
	 * 
	 * @param string $moduleId
	 * @param array  $fields
	 */
	public function __construct($moduleId)
	{
		$this->moduleId  = $moduleId;
		$this->actionUrl = $GLOBALS['APPLICATION']->GetCurPageParam("mid={$this->moduleId}");
	}

	/**
	 * Отрисовывает форму
	 *
	 * @param  \Bitrix\Main\Result  $result
	 * @return string
	 */
	public function render(Result $result = null)
	{
		return ''
			. '<h2>'. Loc::getMessage('IPOLH_DPD_SERVICES_HEAD') .'</h2>'
			. parent::render($result)
			. '<style>[name="IPOLH_DPD_SERVICES"] .adm-detail-content-btns {visibility: hidden;}</style>'
		;
	}

	/**
	 * Поля формы
	 *
	 * @return array
	 */
	public function getFields()
	{
		return [
			[
				'DIV'      => 'IPOLH_DPD_SERVICES_TAB_IMPORT',
				'TAB'      => Loc::getMessage('IPOLH_DPD_SERVICES_TAB_IMPORT'),
				'ICON'     => '',
				'TITLE'    => Loc::getMessage('IPOLH_DPD_SERVICES_TAB_IMPORT_TITLE'),
				'HELP'     => '',
				'OPTIONS'  => [],
				'CONTROLS' => [
					'IMPORT' => [
						'TITLE'        => '',
						'HELP'         => '',
						'SHOW_CAPTION' => 'N',
						'TYPE'         => function() {
							return Loc::getMessage('IPOLH_DPD_SERVICES_IMPORT_RUN');
						}
					]
				]
			],
		];
	}

	/**
	 * Загружаем из БД
	 *
	 * @return void
	 */
	public function loadItem()
	{
		return [];
	}

	/**
	 * Сохраняем в БД
	 *
	 * @return void
	 */
	public function saveItem()
	{}
}