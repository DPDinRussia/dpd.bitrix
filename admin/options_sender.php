<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use \Bitrix\Main\Config\Option;

\Bitrix\Main\Loader::includeModule('ipol.dpd');

$index = isset($_REQUEST['index']) ? $_REQUEST['index'] : null;

$form = new \Ipolh\DPD\Admin\Options\Sender($index);
$form->processAndShow();