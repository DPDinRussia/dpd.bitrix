<?php
use \Bitrix\Main\Config\Option;

$data = [
    'DEFAULT'       => 'Y',
    'NAME'          => 'Default',
    'LOCATION'      => 0,
    'STREET'        => '',
    'STREETABBR'    => '',
    'HOUSE'         => '',
    'KORPUS'        => '',
    'STR'           => '',
    'VLAD'          => '',
    'OFFICE'        => '',
    'FLAT'          => '',
    'TERMINAL_CODE' => '',
];

foreach ($data as $k => $v) {
    $data[$k] = Option::get('ipol.dpd', 'SENDER_'. $k);
}

Option::set('ipol.dpd', 'SENDERS', serialize([0 => $data]));

$GLOBALS['DB']->Query('ALTER TABLE b_ipol_dpd_order ADD COLUMN SENDER varchar(255) NULL');
$GLOBALS['DB']->Query('UPDATE b_ipol_dpd_order SET SENDER = "0"');
$GLOBALS['DB']->Query('ALTER TABLE b_ipol_dpd_order ADD COLUMN USE_MARKING char(1) not null default "N"');