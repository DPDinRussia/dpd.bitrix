<?php
$MESS["IPOLH_DPD_REGION_NAME_TRIM"] = "Область; область; Аобл; аобл; Обл.; обл.; Обл; обл; АО; Республика; республика; Респ; респ; Край; край; автономный округ; г.;";
$MESS["IPOLH_DPD_REGION_NAME_TRIM_MULTI"] = "Автономный округ; автономный округ";
$MESS["IPOLH_DPD_CITY_NAME_MOSCOW"] = "Москва";
$MESS["IPOLH_DPD_CITY_NAME_MOSCOW_CITYES"] = "Москва;Зеленоград;Tвepь;Tверь_969;Московский";
$MESS["IPOLH_DPD_CITY_NAME_PITER"] = "Санкт-Петербург";
$MESS["IPOLH_DPD_CITY_NAME_PITER_CITYES"] = "Санкт-Петербург;Колпино;Красное Село;Кронштадт;Ломоносов;Павловск;Пушкин;Сестрорецк;Петергоф";
$MESS["IPOLH_DPD_CITY_NAME_SEVASTOPOL"] = "Севастополь";
$MESS["IPOLH_DPD_CITY_NAME_SEVASTOPOL_CITYES"] = "Севастополь;Инкерман";
$MESS["IPOLH_DPD_CITY_NAME_TRIM"] = "Город; город; Деревня; деревня; Село; село; Поселок; поселок; Посёлок; посёлок; Станция; станция; Аул; аул; Станица; станица";
$MESS["IPOLH_DPD_CITY_NAME_TRIM_MULTI"] = "Посёлок городского типа; посёлок городского типа";