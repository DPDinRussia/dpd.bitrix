<?php
$MESS["IPOLH_DPD_CITY_NAME_MOSCOW"] = "Москва";
$MESS["IPOLH_DPD_CITY_NAME_MOSCOW_CITYES"] = "Москва;Зеленоград;Tвepь;Tверь_969;Московский";

$MESS["IPOLH_DPD_CITY_NAME_PITER"] = "Санкт-Петербург";
$MESS["IPOLH_DPD_CITY_NAME_PITER_CITYES"] = "Санкт-Петербург;Колпино;Красное Село;Кронштадт;Ломоносов;Павловск;Пушкин;Сестрорецк;Петергоф";

$MESS["IPOLH_DPD_CITY_NAME_SEVASTOPOL"] = "Севастополь";
$MESS["IPOLH_DPD_CITY_NAME_SEVASTOPOL_CITYES"] = "Севастополь;Инкерман";

$MESS["IPOLH_DPD_CITY_NAME_CHUVASHIA"] = "Чувашская";
$MESS["IPOLH_DPD_CITY_NAME_CHUVASHIA_CITYES"] = "Чувашская-Чувашия";

$MESS["IPOLH_DPD_CITY_NAME_HANTY"] = "Ханты-Мансийский";
$MESS["IPOLH_DPD_CITY_NAME_HANTY_CITYES"] = "Ханты-Мансийский- Югра";

$MESS["IPOLH_DPD_CITY_NAME_SEVER_ASETIA"] = "Северная Осетия-Алания";
$MESS["IPOLH_DPD_CITY_NAME_SEVER_ASETIA_CITYES"] = "Северная Осетия - Алания";

$MESS["IPOLH_DPD_CITY_NAME_ALPHABLET_REPLACE"] = "Ё;ё";
$MESS["IPOLH_DPD_CITY_NAME_ALPHABLET_REPLACE_TO"] = "Е;е";

$MESS["IPOLH_DPD_REGION_NAME_TRIM"] = "Область; область; Аобл; аобл; Обл.; обл.; Обл; обл; АО; Республика; республика; Респ; респ; Край; край; автономный округ; г.;";
$MESS["IPOLH_DPD_REGION_NAME_TRIM_MULTI"] = "Автономная область; автономная область; Автономный округ; автономный округ; /Якутия/; (Якутия);- Кузбасс;";
$MESS["IPOLH_DPD_AREA_NAME_TRIM"] = "Район;район";
$MESS["IPOLH_DPD_AREA_NAME_TRIM_MULTI"] = "";
$MESS["IPOLH_DPD_CITY_NAME_TRIM"] = "Город; город; Деревня; деревня; Село; село; Поселок; поселок; Посёлок; посёлок; Станция; станция; Аул; аул; Станица; станица; Агрогородок";
$MESS["IPOLH_DPD_CITY_NAME_TRIM_MULTI"] = "Посёлок городского типа; посёлок городского типа; Рабочий поселок; рабочий поселок; Рабочий посёлок; рабочий посёлок; агрогородок";
$MESS["IPOLH_DPD_CITY_ABBR"] = "Город,город,г,г.";