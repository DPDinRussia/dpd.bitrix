<?php
$MESS["IPOLH_DPD_TABLE_ORDER_NUMBER_INTERNATIONAL"] = "Номер заказа в информационной системе клиента";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_ID"] = "Номер заказа Bitrix";
$MESS["IPOLH_DPD_TABLE_ORDER_SHIPMENT_ID"] = "Номер отгрузки Bitrix";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_NUM"] = "Номер заказа DPD";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_STATUS"] = "Статус заказа DPD";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_STATUS_CANCEL"] = "Статус отмены заказа в DPD";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_ERROR"] = "Ошибки создания заказа DPD";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_DATE"] = "Дата приема груза DPD";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_DATE_BITRIX"] = "Дата приема груза";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_DATE_CREATE"] = "Дата создания заказа";
$MESS["IPOLH_DPD_TABLE_ORDER_ORDER_DATE_CANCEL"] = "Дата отмены заказа";
$MESS["IPOLH_DPD_TABLE_ORDER_DELIVERY_NAME"] = "Служба доставки";
$MESS["IPOLH_DPD_TABLE_ORDER_SERVICE_CODE"] = "Код услуги DPD";
$MESS["IPOLH_DPD_TABLE_ORDER_SERVICE_VARIANT"] = "Вариант доставки";
$MESS["IPOLH_DPD_TABLE_ORDER_CARGO_WEIGHT"] = "Вес отправки, кг";
$MESS["IPOLH_DPD_TABLE_ORDER_CARGO_VOLUME"] = "Объём, м3";
$MESS["IPOLH_DPD_TABLE_ORDER_CARGO_NUM_PACK"] = "Колличество грузомест(посылок)";

$MESS["IPOLH_DPD_TABLE_ORDER_PRICE"] = "Стоимость товаров, руб.";
$MESS["IPOLH_DPD_TABLE_ORDER_PRICE_DELIVERY"] = "Стоимость доставки, руб.";

$MESS["IPOLH_DPD_TABLE_ORDER_CARGO_VALUE"] = "Сумма объявленной ценности, руб.";
$MESS["IPOLH_DPD_TABLE_ORDER_NPP"] = "Наложенный платеж <span style='color:red'>(платная опция)</span>";
$MESS["IPOLH_DPD_TABLE_ORDER_SUM_NPP"] = "Сумма наложенного платежа, руб.";
$MESS["IPOLH_DPD_TABLE_ORDER_CARGO_CATEGORY"] = "Содержимое отправки";
$MESS["IPOLH_DPD_TABLE_ORDER_CARGO_REGISTERED"] = "Ценный груз";
$MESS["IPOLH_DPD_TABLE_ORDER_PICKUP_TIME_PERIOD"] = "Интервал времени приёма груза";
$MESS["IPOLH_DPD_TABLE_ORDER_DELIVERY_TIME_PERIOD"] = "Интервал времени доставки груза";
$MESS["IPOLH_DPD_TABLE_ORDER_TERMINAL_CODE"] = "Код терминала";

$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_LOCATION"] = "Город";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_FIO"] = "Контактное лицо(ФИО)";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_NAME"] = "Имя получателя(название организации или ФИО)";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_PHONE"] = "Телефон";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_EMAIL"] = "Email";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_NEED_PASS"] = "Требовать пропуск";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_COUNTRY"] = "Страна";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_REGION"] = "Регион";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_CITY"] = "Город";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_STREET"] = "Улица";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_STREETABBR"] = "Аббревиатура улицы";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_HOUSE"] = "Дом";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_KORPUS"] = "Корпус";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_VLAD"] = "Владение";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_OFFICE"] = "Офис";
$MESS["IPOLH_DPD_TABLE_ORDER_RECEIVER_FLAT"] = "Квартира";
$MESS["IPOLH_DPD_TABLE_ORDER_COMMENT"] = "Комментарий к заказу";

$MESS["IPOLH_DPD_TABLE_ORDER_SMS_CHECK"] = "SMS уведомление получателя";
$MESS["IPOLH_DPD_TABLE_ORDER_SMS"] = "SMS уведомление получателя о приёме посылки у отправителя и о выходе посылки на доставку";

$MESS["IPOLH_DPD_TABLE_ORDER_EML_CHECK"] = "E-mail уведомление получателя о приёме посылки";
$MESS["IPOLH_DPD_TABLE_ORDER_EML"] = "E-mail уведомление получателя о приёме посылки у отправителя и о выходе посылки на доставку (E-mail)";

$MESS["IPOLH_DPD_TABLE_ORDER_ESD_CHECK"] = "Электронное сообщение о доставке груза получателю";
$MESS["IPOLH_DPD_TABLE_ORDER_ESD"] = "Электронное сообщение о доставке груза получателю (E-mail)";

$MESS["IPOLH_DPD_TABLE_ORDER_ESZ_CHECK"] = "Электронное сообщение о приёме заказа";
$MESS["IPOLH_DPD_TABLE_ORDER_ESZ"] = "Электронное сообщение о приёме заказа (E-mail)";

$MESS["IPOLH_DPD_TABLE_ORDER_OGD"] = "Ожидание на адресе <span style='color:red'>(платная опция)</span>";
$MESS["IPOLH_DPD_TABLE_ORDER_DVD"] = "Доставка в выходные дни. Эта опция, позволяющая осуществить доставку отправки получателю в соответствии со сроком доставки ТК, но раньше даты, рассчитанной по стандартным правилам, при условии, что итоговая дата доставки придется на выходной день <span style='color:red'>(платная опция)</span>";
$MESS["IPOLH_DPD_TABLE_ORDER_VDO"] = "Возврат документов отправителю.
Если клиенту необходимо вернуть сопроводительные документы на груз (товарную накладную, акты приема-передачи), заверенные получателем <span style='color:red'>(платная опция)</span>";

$MESS["IPOLH_DPD_TABLE_ORDER_POD_CHECK"] = "Подтверждение о доставке";
$MESS["IPOLH_DPD_TABLE_ORDER_POD"] = "Подтверждение о доставке (E-mail или факс, на который нужно отправить подтверждение о доставке) <span style='color:red'>(платная опция)</span>";

$MESS["IPOLH_DPD_TABLE_ORDER_PRD"] = "Погрузо-разгрузочные работы при доставке.
Если необходимо организовать разгрузку и доставку поступивших посылок в то или иное помещение на территории, независимо от того, на каком этаже оно расположено <span style='color:red'>(платная опция)</span>";
$MESS["IPOLH_DPD_TABLE_ORDER_TRM"] = "Температурный режим <span style='color:red'>(платная опция)</span>";

$MESS["IPOLH_DPD_TABLE_LABEL_FILE"] = "Печать наклейки";
$MESS["IPOLH_DPD_TABLE_INVOICE_FILE"] = "Печать накладной";

$MESS["IPOLH_DPD_TABLE_PICKUP_DATE_ERROR_EMPTY"] = "Укажите дату отгрузки";
$MESS["IPOLH_DPD_TABLE_PICKUP_DATE_ERROR_LESS"] = "Дата отгрузки не может быть меньше текущей даты";
$MESS["IPOLH_DPD_TABLE_PAYMENT_TYPE"] = "Способ оплаты доставки";

$MESS["IPOLH_DPD_TABLE_ERROR_REQUIRED"] = "Не заполнено обязательное поле \"#NAME#\"";
?>