<?php
$MESS['IPOLH_DPD_ORDER_CREATED'] = 'Заказ на доставку отправлен';
$MESS['IPOLH_DPD_ORDER_CANCELED'] = 'Заказ успешно отменен';
$MESS['IPOLH_DPD_ORDER_LABEL_FILE_OK'] = 'Файл наклеек получен!<br>#LINK#';
$MESS['IPOLH_DPD_ORDER_INVOICE_FILE_OK'] = 'Файл накладной обновлен!<br>#LINK#';
$MESS['IPOLH_DPD_ORDER_RECALCULATE'] = 'Расчитанная стоимость доставки: #PRICE_ACTUAL#<br>Стоимость доставки с учетом скидок и наценок: #PRICE_DELIVERY#';
$MESS['IPOLH_DPD_ORDER_UNKNOWN_ACTION'] = 'Не известное действие!';

$MESS['IPOLH_DPD_ORDER_GOODS_PRICE'] = 'Стоимость товаров с учётом скидок и наценок';
$MESS['IPOLH_DPD_ORDER_DELIVERY_PRICE'] = 'Стоимость доставки с учетом скидок и наценок';
$MESS['IPOLH_DPD_ORDER_TARIFF_PRICE'] = 'Расчитанная стоимость доставки';
$MESS['IPOLH_DPD_ORDER_TARIFF_PRICE_ERROR'] = 'не рассчитанно';
$MESS['IPOLH_DPD_ORDER_PAYED_PRICE'] = 'Уже оплачено';
$MESS['IPOLH_DPD_ORDER_CARGO_VALUE'] = 'Сумма объявленной ценности';
$MESS['IPOLH_DPD_ORDER_NPP'] = 'Наложенный платеж';

$MESS['IPOLH_DPD_ORDER_TAB_1'] = 'Заказ';
$MESS['IPOLH_DPD_ORDER_TAB_1_TITLE'] = 'Информация о заказе';
$MESS['IPOLH_DPD_ORDER_EDIT_NOTE'] = 'Для редактирование данных заказа после передачи в DPD, Вам необходимо отменить текущий заказ, и отправить заявку заново, с новой датой передачи груза в DPD или обратиться к сотруднику DPD по телефону';
$MESS['IPOLH_DPD_ORDER_STATUS'] = 'Cтатус заказа DPD';
$MESS['IPOLH_DPD_ORDER_ID'] = 'Номер заказа Bitrix';
$MESS['IPOLH_DPD_ORDER_NUM'] = 'Номер заказа DPD';
$MESS['IPOLH_DPD_ORDER_SERVICE_CODE'] = 'Служба доставки';
$MESS['IPOLH_DPD_ORDER_LIST_NULL'] = '-- не выбрано --';
$MESS['IPOLH_DPD_PAYMENT_TYPE'] = 'Способ оплаты доставки';
$MESS['IPOLH_DPD_PAYMENT_TYPE_AUTO'] = 'У отправителя по безналичному расчёту';
$MESS['IPOLH_DPD_PAYMENT_TYPE_OUP'] = 'Оплата у получателя наличными';
$MESS['IPOLH_DPD_PAYMENT_TYPE_OUO'] = 'Оплата у отправителя наличными';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT'] = 'Вариант доставки';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_DD'] = 'ДД';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_DD_TITLE'] = 'Дверь - Дверь';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_DT'] = 'ДТ';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_DT_TITLE'] = 'Дверь - Терминал';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_TD'] = 'ТД';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_TD_TITLE'] = 'Терминал - Дверь';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_TT'] = 'ТТ';
$MESS['IPOLH_DPD_ORDER_SERVICE_VARIANT_TT_TITLE'] = 'Терминал - Терминал';
$MESS['IPOLH_DPD_ORDER_PICKUP_DATE'] = 'Дата передачи груза в DPD';

$MESS['IPOLH_DPD_ORDER_INFO'] = 'Детали заказа';
$MESS['IPOLH_DPD_ORDER_CARGO_WEIGHT'] = 'Вес отправки, кг';
$MESS['IPOLH_DPD_ORDER_DIMENSIONS'] = 'Габариты (ШхВхГ), см';
$MESS['IPOLH_DPD_ORDER_CARGO_VOLUME'] = 'Объём, м3';

$MESS['IPOLH_DPD_ORDER_TAB_PAYMENT'] = 'Оплата';
$MESS['IPOLH_DPD_ORDER_TAB_PAYMENT_TITLE'] = 'Оплата заказа';
$MESS['IPOLH_DPD_ORDER_TAB_PAYMENT_HELP'] = '<b style="color: red">Внимание!</b><br>Все суммы указываются в валюте заказа!<br>Стоимость товаров в составе заказа указывается за одну единицу!';
$MESS['IPOLH_DPD_ORDER_SUBHEADER_PAYMENT_CARGO'] = 'Объявленная ценность';
$MESS['IPOLH_DPD_ORDER_CARGO_VALUE'] = 'Сумма объявленной ценности';
$MESS['IPOLH_DPD_ORDER_NPP_CHECK'] = 'Курьер получает деньги за заказ';
$MESS['IPOLH_DPD_ORDER_SUM_NPP'] = 'Сумма наложенного платежа';
$MESS['IPOLH_DPD_ORDER_SUM_NPP_COMMENT'] = 'В заказе установлена инфомация о часточной оплате #SUM#, поэтому сумма нал.платежа уменьшена на эту сумму';

$MESS['IPOLH_DPD_ORDER_TAB_SERVICES'] = 'Опции';
$MESS['IPOLH_DPD_ORDER_TAB_SERVICES_TITLE'] = 'Дополнительные опции';
$MESS['IPOLH_DPD_ORDER_TAB_SERVICES_HELP'] = '<b>Внимание!</b> Стоимость платных опций не учитывается при расчете доставки!';

$MESS['IPOLH_DPD_ORDER_DOCUMENTS'] = 'Документы';
$MESS['IPOLH_DPD_ORDER_DOCUMENTS_TITLE'] = 'Документы для скачивания';
$MESS['IPOLH_DPD_ORDER_DOCUMENTS_HELP'] = 'Документы можно печатать только для созданного заказа в DPD со статусом <b>«Успешно создан»</b>';
$MESS['IPOLH_DPD_ORDER_SUBHEADER_DOCUMENTS_INVOICE'] = 'Накладная';
$MESS['IPOLH_DPD_ORDER_INVOICE_FILE'] = 'Файл накладной';
$MESS['IPOLH_DPD_ORDER_BUTTON_INVOICE_1'] = 'Запросить файл накладной';
$MESS['IPOLH_DPD_ORDER_BUTTON_INVOICE_2'] = 'Обновить файл накладной';
$MESS['IPOLH_DPD_ORDER_SUBHEADER_DOCUMENTS_STICKER'] = 'Печать наклеек';
$MESS['IPOLH_DPD_ORDER_LABEL_FILE'] = 'Файл наклеек';
$MESS['IPOLH_DPD_ORDER_LABEL_FILE_COUNT'] = 'Кол-во наклеек для формирования';
$MESS['IPOLH_DPD_ORDER_LABEL_FILE_FORMAT'] = 'Формат файла';
$MESS['IPOLH_DPD_ORDER_LABEL_FILE_SIZE'] = 'Формат области печати';
$MESS['IPOLH_DPD_ORDER_BUTTON_LABEL_1'] = 'Запросить файл наклеек';
$MESS['IPOLH_DPD_ORDER_BUTTON_LABEL_2'] = 'Обновить файл наклеек';

$MESS['IPOLH_DPD_ORDER_SENDER_DIMENSIONS_ERROR']   = '<span style="color: red">Габариты посылки превышают макс. габариты терминала отправления</span>';
$MESS['IPOLH_DPD_ORDER_RECEIVER_DIMENSIONS_ERROR'] = '<span style="color: red">Габариты посылки превышают макс. габариты терминала назначения</span>';
$MESS['IPOLH_DPD_ORDER_NPP_ERROR'] = '<span style="color: red">Выбранный терминал не может принять наложенный платеж</span>';
$MESS['IPOLH_DPD_ORDER_WITHOUT_VAT'] = 'Без НДС';
$MESS['IPOLH_DPD_ORDER_CARGO_VALUE_CHECK'] = 'Указывать ОЦ';
$MESS['IPOLH_DPD_ORDER_SUBHEADER_PAYMENT_UNIT_LOADS'] = 'Состав заказа';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_NAME'] = 'Наименование';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_QUANTITY'] = 'Кол-во';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_CARGO'] = 'Объявленная ценность';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_NPP'] = 'Наложенный платеж';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_VAT'] = 'НДС';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_GTIN'] = 'Идентификационный номер';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_SERIAL'] = 'Серийный номер';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_RETURN'] = 'Возврат.';
$MESS['IPOLH_DPD_ORDER_PAYMENT_UNIT_RETURN_DESC'] = 'Причина возврата:';
$MESS['IPOLH_DPD_ORDER_PAYMENT_USE_MARKING'] = 'Указать маркировку';
$MESS['IPOLH_DPD_ORDER_SENDER'] = 'Склад';
$MESS['IPOLH_DPD_ORDER_SENDER_OTHER'] = 'Другой';