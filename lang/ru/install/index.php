<?php
$MESS['ipol.dpd_MODULE_NAME'] = 'Интеграция с DPD';
$MESS['ipol.dpd_MODULE_DESCRIPTION'] = '';
$MESS['ipol.dpd_INSTALL_ERROR_TITLE'] = 'Ошибка при установке модуля';
$MESS['ipol.dpd_INSTALL_ERROR_SOAP_NOT_FOUND']   = '<span style="color:red">Внимание!</span> Для работы модуля на сервере необходимо наличие php библиотеки SOAP. Обратитесь за помощью к техподдержке сервера.';
$MESS['ipol.dpd_INSTALL_ERROR_SALE_NOT_FOUND']   = '<span style="color:red">Внимание!</span> Для работы модуля не обходим установленный модуль "Интернет-магазин".';
$MESS['ipol.dpd_INSTALL_ERROR_SALE_BAD_VERSION'] = '<span style="color:red">Внимание!</span> Модуль разработан и корректно функционирует при установке на коробочную сборку 1С-Битрикс версии старше 15.5.0 или при переходе на версиях 15.0 на новую систему управления заказами.';
$MESS['ipol.dpd_DELIVERY_SERVICE_NAME'] = 'Доставка DPD';
$MESS['ipol.dpd_DELIVERY_SERVICE_DESC'] = 'DPD — это одна из крупнейших логистических компаний, которая осуществляет курьерскую доставку посылок по России и СНГ, с собственной сетью пунктов выдачи';
$MESS['ipol.dpd_DELIVERY_SERVICE_DESC_INNER'] = 'Обработчик доставки DPD <a href="http://www.dpd.ru">http://www.dpd.ru</a>.  Доставка только по СНГ.';