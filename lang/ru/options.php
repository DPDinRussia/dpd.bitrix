<?php

/* Tab 1 */
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON"] = "Основные настройки";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TITLE"] = "Основные настройки Доставки DPD";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_HELP"] = "Получить ключ интеграции вы можете пройдя по ссылке <a href=\"https://my.dpd.ru/settings/integration\">https://my.dpd.ru/settings/integration</a> в личном кабинете MyDPD или обратившись в службу поддержки на email integrators@dpd.ru";
$MESS["IPOLH_DPD_OPTIONS_IS_TEST"] = "Тестовый режим";
$MESS["IPOLH_DPD_OPTIONS_IS_TEST_HELP"] = "При установленном флаге все обращения к службе DPD будут направляться на тестовый сервер. В связи с этим полученные значения при расчетах будут отличаться от реальных данных.";
$MESS["IPOLH_DPD_OPTIONS_CLIENT_NUMBER"] = "Клиентский номер в системе DPD";
$MESS["IPOLH_DPD_OPTIONS_CLIENT_NUMBER_HELP"] = "Номер вашего договора с DPD";
$MESS["IPOLH_DPD_OPTIONS_CLIENT_KEY"] = "Ключ для авторизации";
$MESS["IPOLH_DPD_OPTIONS_CLIENT_KEY_HELP"] = "Ваш уникальный ключ для авторизации, полученный у сотрудника DPD";
$MESS["IPOLH_DPD_OPTIONS_SHOW_ADMIN_BUTTON"] = "Отображать кнопку \"Доставка DPD\"";
$MESS["IPOLH_DPD_OPTIONS_SHOW_ADMIN_BUTTON_ONLY_DPD"] = "Только у заказов с доставкой DPD";
$MESS["IPOLH_DPD_OPTIONS_SHOW_ADMIN_BUTTON_ALWAYS"] = "Всегда";

$MESS["IPOLH_DPD_OPTIONS_ORDER_ID"] = "Номер заказа";
$MESS["IPOLH_DPD_OPTIONS_ORDER_ID_ID"] = "ID заказа";
$MESS["IPOLH_DPD_OPTIONS_ORDER_ID_NUMBER"] = "Номер заказа";
$MESS["IPOLH_DPD_OPTIONS_ORDER_ID_REQUIRED"] = "Поле \"Номер заказа\" обязательно для заполнения";
$MESS["IPOLH_DPD_OPTIONS_ORDER_ID_HELP"] = "Выбирается поле заказа, значение которого будет использовано для связи заказа 1С-Битрикс и DPD";

$MESS["IPOLH_DPD_OPTIONS_NOT_INCLUDE_MAP_API"] = "Не подключать API карт";
$MESS["IPOLH_DPD_OPTIONS_NOT_INCLUDE_MAP_API_HELP"] = "Если на странице уже есть подключение API яндекс.карт необходимо выставить эту опцию, чтобы избежать возможные конфликты";

$MESS["IPOLH_DPD_OPTIONS_REQUIRED_IS_SELECT_PVZ"] = "Обязателен ли выбор ПВЗ";

$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU"] = "Россия";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU_TITLE"] = "Параметры авторизации для России";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_RU_HELP"] = "";

$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ"] = "Казахстан";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ_TITLE"] = "Параметры авторизации для Казахстана";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KZ_HELP"] = "";

$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY"] = "Беларусь";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY_TITLE"] = "Параметры авторизации для Беларусии";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_BY_HELP"] = "";

$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG"] = "Киргизия";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG_TITLE"] = "";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_KG_HELP"] = "";

$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM"] = "Армения";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM_TITLE"] = "";
$MESS["IPOLH_DPD_OPTIONS_TAB_COMMON_TAB_AM_HELP"] = "";

$MESS["IPOLH_DPD_OPTIONS_API_DEF_COUNTRY"] = "Аккаунт по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_API_DEF_COUNTRY_HELP"] = "Выберите основной аккаунт сайта";

$MESS["IPOLH_DPD_OPTIONS_KLIENT_CURRENCY"] = "Валюта";
$MESS["IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_RU_HELP"] = "Выберите валюту соответствующую российскому рублю";
$MESS["IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_KZ_HELP"] = "Выберите валюту соответствующую казахстанскому тенге";
$MESS["IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_BY_HELP"] = "Выберите валюту соответствующую белорусскому рублю";

$MESS["IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_KG_HELP"] = "Выберите валюту соответствующую киргизскому сому";
$MESS["IPOLH_DPD_OPTIONS_KLIENT_CURRENCY_AM_HELP"] = "Выберите валюту соответствующую армянскому драму";


/* Tab 2 */
$MESS["IPOLH_DPD_OPTIONS_TAB_SENDER"] = "Отправитель";
$MESS["IPOLH_DPD_OPTIONS_TAB_SENDER_TITLE"] = "Данные отправителя";
$MESS["IPOLH_DPD_OPTIONS_SENDER_FIO"] = "Контактное лицо";
$MESS["IPOLH_DPD_OPTIONS_SENDER_FIO_HELP"] = "ФИО";
$MESS["IPOLH_DPD_OPTIONS_SENDER_NAME"] = "Имя отправителя";
$MESS["IPOLH_DPD_OPTIONS_SENDER_NAME_HELP"] = "Название организации или ФИО";
$MESS["IPOLH_DPD_OPTIONS_SENDER_PHONE"] = "Телефон отправителя";
$MESS["IPOLH_DPD_OPTIONS_SENDER_EMAIL"] = "Email отправителя";
$MESS["IPOLH_DPD_OPTIONS_SENDER_EMAIL_REQUIRED"] = "Поле \"Email отправителя\" обязательно для заполнения";
$MESS["IPOLG_DPD_OPTIONS_SENDER_REGULAR_NUM"] = "Номер регулярного отправления";
$MESS["IPOLH_DPD_OPTIONS_SENDER_NEED_PASS"] = "Требовать пропуск";
$MESS["IPOLH_DPD_OPTIONS_SENDER_ADDRESS_SUBHEADER"] = "Адрес";
$MESS["IPOLH_DPD_OPTIONS_SENDER_NAME"] = "Наименование профиля";
$MESS["IPOLH_DPD_OPTIONS_SENDER_DEFAULT"] = "По умоланию";
$MESS["IPOLH_DPD_OPTIONS_SENDER_DEFAULT_HELP"] = "Данный адрес будет использован при расчете доставки на сайте";
$MESS["IPOLH_DPD_OPTIONS_SENDER_LOCATION"] = "Город";
$MESS["IPOLH_DPD_OPTIONS_SENDER_LOCATION_HELP"] = "Город-отправитель выставляется в настройках Интернет-магазина в закладке 'Адрес магазина' -> Местоположение магазина.";
$MESS["IPOLH_DPD_OPTIONS_SENDER_LOCATION_GROUPS"] = "Группы-местоположений";
$MESS["IPOLH_DPD_OPTIONS_SENDER_LOCATION_GROUPS_HELP"] = "С помощью данной настройки можно указать группу местоположений, для которых данный отправитель будет применяться автоматически";
$MESS["IPOLH_DPD_OPTIONS_SENDER_SUBTAB_COURIER"] = "Дверь";
$MESS["IPOLH_DPD_OPTIONS_SENDER_SUBTAB_COURIER_TITLE"] = "При отправке курьером";
$MESS["IPOLH_DPD_OPTIONS_SENDER_STREET"] = "Улица";
$MESS["IPOLH_DPD_OPTIONS_SENDER_STREETABBR"] = "Аббревиатура улицы";
$MESS["IPOLH_DPD_OPTIONS_SENDER_STREETABBR_HELP"] = "ул, пр-т, б-р и т.д.";
$MESS["IPOLH_DPD_OPTIONS_SENDER_HOUSE"] = "Дом";
$MESS["IPOLH_DPD_OPTIONS_SENDER_KORPUS"] = "Корпус";
$MESS["IPOLH_DPD_OPTIONS_SENDER_STR"] = "Строение";
$MESS["IPOLH_DPD_OPTIONS_SENDER_VLAD"] = "Владение";
$MESS["IPOLH_DPD_OPTIONS_SENDER_OFFICE"] = "Офис";
$MESS["IPOLH_DPD_OPTIONS_SENDER_FLAT"] = "Квартира";
$MESS["IPOLH_DPD_OPTIONS_SENDER_SUBTAB_PICKUP"] = "Терминал";
$MESS["IPOLH_DPD_OPTIONS_SENDER_SUBTAB_PICKUP_HELP"] = "При отправке от терминала";
$MESS["IPOLH_DPD_OPTIONS_SENDER_TERMINAL_CODE"] = "Терминал";

/* Tab 3 */
$MESS["IPOLH_DPD_OPTIONS_TAB_RECEIVER"] = "Получатель";
$MESS["IPOLH_DPD_OPTIONS_TAB_RECEIVER_TITLE"] = "Свойства получателя заказа";
$MESS["IPOLH_DPD_OPTIONS_TAB_RECEIVER_HELP"] = "При создании заказа DPD Вы заполняете поля анкеты, чтобы каждый раз не заполнять их в ручную, Вы можете автоматически подгружать эти данные из заказа Битрикс. Для этого привяжите соответсвующие свойства заказа Битрикс";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_CUSTOM"] = "- Нет привязки -";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_FIO"] = "Контактное лицо";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_NAME"] = "Название получателя";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_PHONE"] = "Контактный телефон";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_EMAIL"] = "Почтовый ящик, email";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_LOCATION"] = "Страна / регион / город (местоположение)";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_CITYALT"] = "Альтернативный город";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_STREET"] = "Улица";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_STREETABBR"] = "Аббревиатура улицы";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_STREETABBR_HELP"] = "ул, пр-т, б-р и т.д.";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_HOUSE"] = "Дом";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_KORPUS"] = "Корпус";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_STR"] = "Строение";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_VLAD"] = "Владение";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_OFFICE"] = "Офис";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_FLAT"] = "Квартира";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_PVZ_FIELD"] = "Cвойство, куда будет сохранен выбранный пункт самовывоза";
$MESS['IPOLH_DPD_OPTIONS_RECEIVER_COMMENT'] = 'Комментарий к заказу';
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_NEED_PASS"] = "Требовать пропуск";

/* Tab 4 */
$MESS["IPOLH_DPD_OPTIONS_TAB_CALCULATE"] = "Расчет доставки";
$MESS["IPOLH_DPD_OPTIONS_TAB_CALCULATE_TITLE"] = "Настройки расчета доставки";
$MESS["IPOLH_DPD_OPTIONS_EXCLUDE_TARIFF"] = "Укажите тарифы которые <b>НЕ будут</b><br>использоваться при расчетах";

$MESS["IPOLH_DPD_OPTIONS_DEFAULT_TARIFF_CODE"] = "Тариф по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_DEFAULT_TARIFF_CODE_HELP"] = "Данный тариф будет выбран автоматически если расчитанная стоимость доставки будет меньше указанной ниже";
$MESS["IPOLH_DPD_OPTIONS_DEFAULT_TARIFF_THRESHOLD"] = "Максимальная стоимость доставки<br>при которой будет применен тариф по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_DECLARED_VALUE"] = "Включать страховку в стоимость доставки";
$MESS["IPOLH_DPD_OPTIONS_DECLARED_VALUE_HELP"] = "Объявленная ценность посылки, равна стоимости товаров в корзине";

$MESS["IPOLH_DPD_OPTIONS_CALCULATE_BY_PARCEL"] = "Расчитывать стоимость доставки по товарам";
$MESS["IPOLH_DPD_OPTIONS_CALCULATE_BY_PARCEL_HELP"] = "При расчете стоимости доставки будет использован механизм расчета по посылкам";

$MESS["IPOLH_DPD_OPTIONS_CALCULATE_ROUNDING"] = "Округлять до единиц";
$MESS["IPOLH_DPD_OPTIONS_CALCULATE_ROUNDING_HELP"] = "Округление работает до единиц в большую сторону. Например если в поле указано число 5, то округление будет идти до 5 рублей, например 242 = 245, а 246 - 250.";
$MESS["IPOLH_DPD_OPTIONS_ADD_DELIVERY_DAY"] = "Увеличить срок доставки на (дн.)";
$MESS["IPOLH_DPD_OPTIONS_DEFAULT_PRICE"] = "Стоимость доставки по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_DEFAULT_PRICE_HELP"] = "Если указана, то стоимость доставки при расчете будет фиксированной";

$MESS["IPOLH_DPD_OPTIONS_COMMISSION_HEADER"] = "Комиссия наложенного платежа";
$MESS["IPOLH_DPD_OPTIONS_COMMISSION_HELP"] = "Данная группа настроек позволяет указать наценку на стоимость доставки при условии, что оплата будет происходить наложенным платежом.";
$MESS["IPOLH_DPD_OPTIONS_COMMISSION_NPP_CHECK"] = "Включать комиссию за инкассацию наложенным платежом в стоимость доставки";

$MESS["IPOLH_DPD_OPTIONS_COMMISSION_NPP_PERSENT"] = "Комиссия от стоимости товаров в заказе (в процентах), %";
$MESS["IPOLH_DPD_OPTIONS_COMMISSION_NPP_MINSUMM"] = "Минимальная сумма комиссии, руб.";
$MESS["IPOLH_DPD_OPTIONS_COMMISSION_NPP_PAYMENT"] = "Привяжите платежные системы, которые означают что оплата будет происходить наложенным платежом";
$MESS["IPOLH_DPD_OPTIONS_COMMISSION_NPP_DEFAULT"] = "Если платежную систему определить не удалось, считать ли что оплата будет происходить наложенным платежом по умолчанию?";
$MESS["IPOLH_DPD_OPTIONS_COMMISSION_NPP_DEFAULT_HELP"] = "В некоторых случаях модулю не удается определить выбранную платежную систему пользователем. Данная настройка позволяет определить поведения модуля в таких случаях";


/* Tab 5 */
$MESS["IPOLH_DPD_OPTIONS_TAB_LOCATION"] = "Местоположения";
$MESS["IPOLH_DPD_OPTIONS_TAB_LOCATION_TITLE"] = "Настройки местоположений";
$MESS["IPOLH_DPD_OPTIONS_TAB_LOCATION_HELP"] = "Для возможности автоматического расчета стоимости доставки DPD нужно правильно настроить типы местоположений. Выберите одноименные типы в списках соответствующие названиям [REGION], [CITY], [VILLAGE]. Если у вас типы местоложений называются по другому, то привяжите их по вашим названиям.";
$MESS["IPOLH_DPD_OPTIONS_LOCATION_COUNTRY"] = "Тип местоположений регион [COUNTRY]";
$MESS["IPOLH_DPD_OPTIONS_LOCATION_REGION"] = "Тип местоположений регион [REGION]";
$MESS["IPOLH_DPD_OPTIONS_LOCATION_AREA"] = "Тип местоположений район [SUBREGION]";
$MESS["IPOLH_DPD_OPTIONS_LOCATION_CITY"] = "Тип местоположений город [CITY]";
$MESS["IPOLH_DPD_OPTIONS_LOCATION_VILLAGE"] = "Тип местоположений поселок [VILLAGE]";

/* Tab 6 */
$MESS["IPOLH_DPD_OPTIONS_TAB_OPTIONS"] = "Описание отправки";
$MESS["IPOLH_DPD_OPTIONS_TAB_OPTIONS_TITLE"] = "Задайте значения по умолчанию, которые всегда можно будет изменить при создании заказа";
$MESS["IPOLH_DPD_OPTIONS_TAB_OPTIONS_HELP"] = "Стоимость опций не учитывается при расчете доставки. Если хотите их учесть, Вам необходимо выставить наценку вручную, на странице редактирования параметров службы доставки. Для этого перейдите \"<a href=\"/bitrix/admin/sale_delivery_service_list.php?lang=ru&filter_group=0\" target=\"_blank\">Магазин -> Настройки -> Службы доставки -> DPD</a>\", вкладка \"Настройки обработчика\"";
$MESS["IPOLH_DPD_OPTIONS_SELF_PICKUP"] = "Способ отправления";
$MESS["IPOLH_DPD_OPTIONS_SELF_PICKUP_YES"] = "Мы будем возить заказы сами";
$MESS["IPOLH_DPD_OPTIONS_SELF_PICKUP_NO"] = "Мы хотим вызывать забор автоматически";
$MESS["IPOLH_DPD_OPTIONS_SELF_PICKUP_HELP"] = "Стоимость забора посылок от двери составляет 295 руб.";
$MESS["IPOLH_DPD_OPTIONS_PAYMENT_TYPE"] = "Способ оплаты доставки";
$MESS["IPOLH_DPD_OPTIONS_PAYMENT_TYPE_AUTO"] = "У отправителя по безналичному расчёту";
$MESS["IPOLH_DPD_OPTIONS_PAYMENT_TYPE_OUP"] = "Оплата у получателя наличными";
$MESS["IPOLH_DPD_OPTIONS_PAYMENT_TYPE_OUO"] = "Оплата у отправителя наличными";
$MESS["IPOLH_DPD_OPTIONS_PAYMENT_TYPE_HELP"] = "";
$MESS["IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD"] = "Интервал времени передачи груза в DPD";
$MESS["IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_9_18"] = "в любое время с 09:00 до 18:00";
$MESS["IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_9_13"] = "с 09:00 до 13:00";
$MESS["IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_13_18"] = "с 13:00 до 18:00";
$MESS["IPOLH_DPD_OPTIONS_PICKUP_TIME_PERIOD_14_15"] = "с 14:00 до 15:00";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD"] = "Интервал времени доставки груза";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_9_18"] = "в любое время с 09:00 до 18:00";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_9_14"] = "с 09:00 до 14:00";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_13_18"] = "с 13:00 до 18:00";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_18_22"] = "с 18:00 до 22:00 (оплачивается дополнительно)";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_TIME_PERIOD_19_23"] = "с 19:00 до 23:00";


$MESS["IPOLH_DPD_OPTIONS_CARGO_NUM_PACK"] = "Колличество грузомест(посылок)";
$MESS["IPOLH_DPD_OPTIONS_CARGO_CATEGORY"] = "Содержимое отправки";
$MESS["IPOLH_DPD_OPTIONS_CARGO_CATEGORY_DEFAULT"] = "Текстиль";
$MESS["IPOLH_DPD_OPTIONS_TAB_OPTIONS_SUBTAB_OPTIONS"] = "Опции";
$MESS["IPOLH_DPD_OPTIONS_CARGO_REGISTERED"] = "Ценный груз";
$MESS["IPOLH_DPD_OPTIONS_CARGO_REGISTERED_HELP"] = "Мобильные телефоны, ноутбуки, планшеты";
$MESS["IPOLH_DPD_OPTIONS_DVD"] = "Доставка в выходные дни";
$MESS["IPOLH_DPD_OPTIONS_DVD_HELP"] = "Эта опция, позволяющая осуществить доставку отправки получателю в соответствии со сроком доставки ТК, но раньше даты, расчитанной по стандартным правилам, при условии, что итоговая дата доставки придется на выходной день";
$MESS["IPOLH_DPD_OPTIONS_PAID_COMMENT"] = "<i style=\"color: red\">опция платная, стоимость уточняйте у менеджера</i>";
$MESS["IPOLH_DPD_OPTIONS_TRM"] = "Температурный режим";
$MESS["IPOLH_DPD_OPTIONS_TRM_CODE"] = "ТРМ";
$MESS["IPOLH_DPD_OPTIONS_PRD"] = "Погрузо-разгрузочные работы при доставке";
$MESS["IPOLH_DPD_OPTIONS_PRD_HELP"] = "Если необходимо организовать разгрузку и доставку поступивших посылок в то или иное помещение на территории, независимо от того, на каком этаже оно расположено";
$MESS["IPOLH_DPD_OPTIONS_VDO"] = "Возврат документов отправителю";
$MESS["IPOLH_DPD_OPTIONS_VDO_HELP"] = "Если клиенту необходимо вернуть сопроводительные документы на груз (товарную накладную, акты приема-передачи), заверенные получателем";
$MESS["IPOLH_DPD_OPTIONS_OGD"] = "Ожидание на адресе";
$MESS["IPOLH_DPD_OPTIONS_CHST"] = "Частичный выкуп";
$MESS["IPOLH_DPD_OPTIONS_CHST_COMMENT"] = "Доступно только при доставки до Двери";
$MESS["IPOLH_DPD_OPTIONS_CHST_CODE"] = "ЧСТ";
$MESS["IPOLH_DPD_OPTIONS_GOODS_RETURN_AMOUNT"] = "Минимальная сумма выкупа, при достижении которой доставка будет бесплатной";
$MESS["IPOLH_DPD_OPTIONS_GOODS_RETURN_AMOUNT_COMMENT"] = "Доступно только при наложенном платеже";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_AMOUNT"] = "Сумма за доставку";
$MESS["IPOLH_DPD_OPTIONS_DELIVERY_AMOUNT_COMMENT"] = "Доступно только при наложенном платеже";
$MESS["IPOLH_DPD_OPTIONS_OGD_CODE"] = "ОЖД";
$MESS["IPOLH_DPD_OPTIONS_OGD_EMPTY"] = "- Не установленно -";
$MESS["IPOLH_DPD_OPTIONS_OGD_VNESH"] = "ВНЕШ";
$MESS["IPOLH_DPD_OPTIONS_OGD_VNESH_TITLE"] = "Проверка на внешние повреждения";
$MESS["IPOLH_DPD_OPTIONS_OGD_PRIM"] = "ПРИМ";
$MESS["IPOLH_DPD_OPTIONS_OGD_PRIM_TITLE"] = "Примерка";
$MESS["IPOLH_DPD_OPTIONS_OGD_PROS"] = "ПРОС";
$MESS["IPOLH_DPD_OPTIONS_OGD_PROS_TITLE"] = "Простая";
$MESS["IPOLH_DPD_OPTIONS_OGD_RAB"] = "РАБТ";
$MESS["IPOLH_DPD_OPTIONS_OGD_RAB_TITLE"] = "Проверка работоспособности";
$MESS["IPOLH_DPD_OPTIONS_OGD_SOOT"] = "СООТ";
$MESS["IPOLH_DPD_OPTIONS_OGD_SOOT_TITLE"] = "Проверка на соответствие";
$MESS["IPOLH_DPD_OPTIONS_TAB_OPTIONS_SUBTAB_NOTIFY"] = "Уведомления";
$MESS["IPOLH_DPD_OPTIONS_SMS"] = "SMS уведомление получателя о приёме посылки у отправителя и о выходе посылки на доставку";
$MESS["IPOLH_DPD_OPTIONS_SMS_HELP"] = "номер телефона";
$MESS["IPOLH_DPD_OPTIONS_EML"] = "'E-mail уведомление получателя о приёме посылки у отправителя и о выходе посылки на доставку'";
$MESS["IPOLH_DPD_OPTIONS_ESD"] = "Электронное сообщение о доставке груза получателю";
$MESS["IPOLH_DPD_OPTIONS_ESZ"] = "Электронное сообщение о приёме заказа";
$MESS["IPOLH_DPD_OPTIONS_ESZ_HELP"] = "Укажите адрес эл-ой почты, на который будет отправленно сообщение о приеме заказа ТК";
$MESS["IPOLH_DPD_OPTIONS_POD"] = "Подтверждение о доставке";
$MESS["IPOLH_DPD_OPTIONS_POD_HELP"] = "E-mail или факс, на который нужно отправить подтверждение о доставке";

/* Tab DIMENSIONS */
$MESS["IPOLH_DPD_OPTIONS_TAB_DIMENSIONS"] = "Габариты";
$MESS["IPOLH_DPD_OPTIONS_TAB_DIMENSIONS_TITLE"] = "Задайте значения по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_TAB_DIMENSIONS_HELP"] = "
Данная группа настроек предназначена для определения габаритов тех заказов, где присутствуют товары без заполненных размеров и/или веса. 
Здесь можно задать те значения, что будут браться по умолчанию для заказа или товара.
<br><br>
<b>Применять для всего заказа</b><br>
Будет произведен суммарный расчет габаритов всего заказа, проверяется общий размер и вес заказа и берется то значение - расчитанное или же заданное по умолчанию - которое больше.<br><br>
<b>Применять для товаров в заказе</b>
В этом случае при расчете габаритов, если у товара не задано значение одного или нескольких габаритов, оно будет браться из значений по умолчанию.
<br><br>
<b style=\"color:red\">Внимание!</b> Все габариты берутся из настроек торгового каталога.";
$MESS["IPOLH_DPD_OPTIONS_WEIGHT"] = "Вес заказа по умолчанию, г";
$MESS["IPOLH_DPD_OPTIONS_LENGTH"] = "Длина заказа по умолчанию, мм";
$MESS["IPOLH_DPD_OPTIONS_WIDTH"] = "Ширина заказа по умолчанию, мм";
$MESS["IPOLH_DPD_OPTIONS_HEIGHT"] = "Высота заказа по умолчанию, мм";

$MESS["IPOLH_DPD_OPTIONS_USE_MODE"] = "Применять для";
$MESS["IPOLH_DPD_OPTIONS_USE_MODE_ORDER"] = "Для всего заказа";
$MESS["IPOLH_DPD_OPTIONS_USE_MODE_ITEM"] = "Для товаров в заказе";


// /* Tab STATUS */
$MESS["IPOLH_DPD_OPTIONS_TAB_STATUS"] = "Статус";
$MESS["IPOLH_DPD_OPTIONS_TAB_STATUS_TITLE"] = "Отслеживание статуса заказов DPD";
$MESS["IPOLH_DPD_OPTIONS_TAB_STATUS_HELP"] = "
	Данная группа настроек нужна для того, чтобы оперативно отслеживать статусы заказов. 
	Раз в 10 минут запрашивается информация по статусам отправленных заявок. При получении ответа заказы выставятся 
	в указанные статусы если они приняты, или по каким-то причинам отклонены. Так же отслеживаются статусы доставки заказов. 
	Рекомендуется создать два новых статуса заказа, чтобы удобнее было отслеживать по ним состояние заявок, а так же задать специальные правила в <a href=\"/bitrix/admin/type_admin.php\" target=\"_blank\">Типах почтовых событий</a>, 
	чтобы отсылать письма о смене статусов заказа только менеджерам магазина, а не покупателям.
	<br><br>
	<b style=\"color:red\">Внимание!</b> Для возможности отслеживания статусов посылок DPD Вам необходимо сделать запрос на почту
	<a href=\"mailto:integrators@dpd.ru\" target=\"_blank\">integrators@dpd.ru</a>, 
	с просьбой подключения метода getEvents. В запросе обязательно укажите свой клиентский номер.
";
$MESS["IPOLH_DPD_OPTIONS_SET_TRACKING_NUMBER"] = "Выставлять принятым заказам идентификатор отправления";
$MESS["IPOLH_DPD_OPTIONS_MARK_PAYED"] = "Отмечать доставленный заказ оплаченным";
$MESS["IPOLH_DPD_OPTIONS_MARK_PAYED_HELP"] = "Позволяет выставлять флаг оплаты тем заказам, которые были доставлены. Это актуально для оперативного учета накопительных скидок";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_CHECK"] = "Отслеживать статусы заказов в DPD";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_PICKUP"] = "Статус заказа доставленного на склад";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_TRANSIT"] = "Статус заказа находящегося в пути";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_READY_PICKUP"] = "Статус заказа готового к выдачи на пункте выдачи";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_READY"] = "Статус заказа готового к передаче курьеру для доставки до двери";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_COURIER"] = "Статус заказа переданного курьеру для доставки до двери";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_DELIVERED"] = "Статус заказа выданного получателю";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_PROBLEM"] = "Статус заказа с которым возникла проблемная ситуация";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_CANCEL"] = "Статус заказа от которого отказался клиент";
$MESS["IPOLH_DPD_OPTIONS_STATUS_ORDER_EMPTY"] = "- Нет привязки -";

$MESS["IPOLH_DPD_OPTIONS_CLIENT_NUMBER_REQUIRED"] = "Не указан клиентский номер в системе DPD";
$MESS["IPOLH_DPD_OPTIONS_CLIENT_KEY_REQUIRED"] = "Не указан ключ для авторизации";
$MESS["IPOLH_DPD_OPTIONS_WEIGHT_REQUIRED"] = "Не указан вес заказа по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_LENGTH_REQUIRED"] = "Не указана длина заказа по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_WIDTH_REQUIRED"] = "Не указана ширина заказа по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_HEIGHT_REQUIRED"] = "Не указана высота заказа по умолчанию";
$MESS["IPOLH_DPD_OPTIONS_SENDER_FIO_REQUIRED"] = "Не указано контактное лицо отправителя";
$MESS["IPOLH_DPD_OPTIONS_SENDER_NAME_REQUIRED"] = "Не указано имя отправителя";
$MESS["IPOLH_DPD_OPTIONS_SENDER_PHONE_REQUIRED"] = "Не указан телефон отправителя";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_FIO_REQUIRED"] = "Не указано контактное лицо получателя (#PERSONE_TYPE_NAME# [#PERSON_TYPE_ID#])";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_NAME_REQUIRED"] = "Не указано название получателя (#PERSONE_TYPE_NAME# [#PERSON_TYPE_ID#])";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_PHONE_REQUIRED"] = "Не указан контактный телефон получателя (#PERSONE_TYPE_NAME# [#PERSON_TYPE_ID#])";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_EMAIL_REQUIRED"] = "Не указан email получателя (#PERSONE_TYPE_NAME# [#PERSON_TYPE_ID#])";
$MESS["IPOLH_DPD_OPTIONS_RECEIVER_LOCATION_REQUIRED"] = "Не указано местоположение получателя (#PERSONE_TYPE_NAME# [#PERSON_TYPE_ID#])";
$MESS["IPOLH_DPD_OPTIONS_CARGO_CATEGORY_REQUIRED"] = "Не указано описание отправки";

// /* Tab FAQ */
$MESS["IPOLH_DPD_OPTIONS_TAB_FAQ"] = "FAQ";
$MESS["IPOLH_DPD_OPTIONS_TAB_FAQ_TITLE"] = "Инструкция по работе с модулем";
$MESS["IPOLH_DPD_OPTIONS_TAB_FAQ_CHECKER"] = '<p><a href="/bitrix/js/ipol.dpd/server-settings-checker.php" target="_blank">Проверка параметров сервера</a></p>';
$MESS["IPOLH_DPD_OPTIONS_TAB_FAQ_HELP"] = '<p><a href="https://ipol.ru/spravka/dpd_bitrix/about_plugin/" target="_blank">Инструкция по работе с модулем</a></p>';
/**********************************************************************************************************************/

$MESS['IPOLH_DPD_SERVICES_HEAD'] = 'Служебные процедуры';
$MESS['IPOLH_DPD_SERVICES_TAB_IMPORT'] = 'Импорт данных';
$MESS['IPOLH_DPD_SERVICES_TAB_IMPORT_TITLE'] = 'Импорт внешних данных';
$MESS['IPOLH_DPD_SERVICES_IMPORT_NOT_RUNING'] = '<b>Внимание!</b><br>Для правильной работы модуля необходимо произвести загрузку внешних данных.<br><br>';
$MESS['IPOLH_DPD_SERVICES_IMPORT_RUN'] = '<input type="button" onclick="document.location.href=\'/bitrix/admin/ipolh_dpd_load_external_data.php\'" class="adm-btn-save" value="Запустить импорт">';

$MESS['IPOLH_DPD_SERVICES_TAB_SENDER'] = 'Параметры';
$MESS['IPOLH_DPD_SERVICES_TAB_SENDER_TITLE'] = 'Редактирование отправителя';


$MESS['IPOLH_DPD_OPTIONS_DEBUG_HEADER'] = 'Отладка';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL'] = 'Уровень журналирования';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_HELP'] = 'Указывается уровень сообщений которые будут записываться в log-файл';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_NONE'] = 'Отключено';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_DEBUG'] = 'Отладочная информация';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_NOTICE'] = 'Уведомления';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_WARNING'] = 'Предупреждения';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_ERROR'] = 'Ошибки';
$MESS['IPOLH_DPD_OPTIONS_LOG_LEVEL_ALL'] = 'Все';
$MESS['IPOLH_DPD_OPTIONS_LOG_FILE_PATH'] = 'Путь к log-файлу';

$MESS['IPOLH_DPD_OPTIONS_SENDER_EDIT'] = "Редактировать";
$MESS['IPOLH_DPD_OPTIONS_SENDER_ADD'] = "Добавить";
$MESS['IPOLH_DPD_OPTIONS_SENDER_NAME'] = 'Наименование';
$MESS['IPOLH_DPD_OPTIONS_SENDER_LOCATION'] = 'Местоположение';
$MESS['IPOLH_DPD_OPTIONS_SENDER_DEFAULT'] = 'По умолчанию';

$MESS['SAME_DAY_WARNING'] = 'Внимание! Услуга DPD Same Day поддерживает не все опции. Подробнее уточните у вашего менеджера DPD.';

$MESS['IPOLH_DPD_OPTIONS_TYPE_DISPLAYED_PVZ'] = 'Тип ПВЗ, который будут скрыт на карте';
$MESS['IPOLH_DPD_OPTIONS_TERMINAL_TYPE_PVP_KEY'] = 'ПВП';
$MESS['IPOLH_DPD_OPTIONS_TERMINAL_TYPE_PVP_NAME'] = 'Пункт выдачи';
$MESS['IPOLH_DPD_OPTIONS_TERMINAL_TYPE_POSTOMAT_KEY'] = 'П';
$MESS['IPOLH_DPD_OPTIONS_TERMINAL_TYPE_POSTOMAT_NAME'] = 'Постамат';
