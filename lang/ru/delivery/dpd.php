<?php
$MESS['IPOLH_DPD_PROFILE_DESCRIPTION'] = '';
$MESS['IPOLH_DPD_PROFILE_DESCRIPTION_INNER'] = 'Обработчик доставки DPD <a href="http://www.dpd.ru">http://www.dpd.ru</a>.  Доставка только по СНГ.';
$MESS['IPOLH_DPD_PROFILE_PICKUP_TITLE'] = 'Доставка до пункта выдачи';
$MESS['IPOLH_DPD_PROFILE_PICKUP_DESCRIPTION'] = 'доставка до пункта выдачи DPD и/или почтомата';
$MESS['IPOLH_DPD_PROFILE_COURIER_TITLE'] = 'Доставка до двери';
$MESS['IPOLH_DPD_PROFILE_COURIER_DESCRIPTION'] = 'доставка посылки курьером до двери получателя';
$MESS["IPOLH_DPD_DELIVERY_ERROR"] = "Служба не имеет доставки в город или сервис доставки времено не отвечает!";
$MESS["IPOLH_DPD_DELIVERY_DAY"] = "дн. #DAYS# (#TARIFF_NAME#)";
$MESS["IPOLH_DPD_DELIVERY_SELECT_TERMINAL"] = "Выбрать ПВЗ";