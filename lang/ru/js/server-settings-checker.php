<?php
$MESS['IPOLH_DPD_NO'] = 'Нет';
$MESS['IPOLH_DPD_YES'] = 'Да';
$MESS['IPOLH_DPD_PARAMETER'] = 'Создать заказ';
$MESS['IPOLH_DPD_INFO_MES1'] = 'Для работы модуля необходимо чтобы запросы на внешние ресурсы не приводили к ошибкам. В случае проблем необходимо обратиться  к хостеру.';
$MESS['IPOLH_DPD_INFO_MES2'] = 'Есть доступ к внешним ресурсам';
$MESS['IPOLH_DPD_INFO_MES3'] = 'Поддержка SOAP';
$MESS['IPOLH_DPD_INFO_MES4'] = 'Для работы модуля требуется чтобы значение времени ожидания для потоков, использующих сокеты выделяемой памяти было более 600 секунд. За это отвечает параметр <a href="https://www.php.net/manual/ru/filesystem.configuration.php#ini.default-socket-timeout">default_socket_timeout</a>';
$MESS['IPOLH_DPD_INFO_MES5'] = 'Для работы модуля требуется, чтобы он мог корректно определить максимальное время выполнения скриптов. За это отвечает параметр <a href="https://www.php.net/manual/ru/info.configuration.php#ini.max-execution-time">max_execution_time</a>.';
$MESS['IPOLH_DPD_INFO_MES6'] = 'Установлен';
$MESS['IPOLH_DPD_INFO_MES7'] = 'Требуется версия php 5.6 и выше';
$MESS['IPOLH_DPD_INFO_MES8'] = 'Версия PHP';
$MESS['IPOLH_DPD_INFO_MES9'] = 'Примечание';
$MESS['IPOLH_DPD_INFO_MES10'] = 'Текущее значение';
$MESS['IPOLH_DPD_INFO_MES11'] = 'Требуется';
$MESS['IPOLH_DPD_INFO_MES12'] = 'Параметр';
$MESS['IPOLH_DPD_INFO_MES13'] = 'Для работы модуля требуется установленное расширение <a href="https://www.php.net/manual/ru/book.soap.php">PHP-SOAP</a> и доступен класс <a href="https://www.php.net/manual/ru/class.soapclient.php">\SoapClient</a>';
$MESS['IPOLH_DPD_INFO_MES14'] = 'Для работы модуля требуется #MEMORY#М выделяемой памяти. За это отвечает параметр <a href="https://www.php.net/manual/ru/ini.core.php#ini.memory-limit">memory_limit</a>';
$MESS['IPOLH_DPD_INFO_MES15'] = 'В случае, если какой-либо параметр не соответствует требованиям - необходимо самостоятельно установить ему требуемое значение в настройках вашего сервера либо обратиться к специалистам.';