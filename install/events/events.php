<?php
return array(
	array(
		'module'   => 'sale',
		'name'     => 'onSaleDeliveryHandlersBuildList',
		'callback' => array('\\Ipolh\\DPD\\Delivery\\DPD', 'Init'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleComponentOrderOneStepProcess',
		'callback' => array('\\Ipolh\\DPD\\Delivery\\DPD', 'OnSaleComponentOrderOneStepProcess'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleComponentOrderOneStepPaySystem',
		'callback' => array('\\Ipolh\\DPD\\Delivery\\DPD', 'OnSalePaySystemFilter'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleComponentOrderOneStepDelivery',
		'callback' => array('\\Ipolh\\DPD\\Delivery\\DPD', 'OnSaleDeliveryFilter'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleComponentOrderOneStepDelivery',
		'callback' => array('\\Ipolh\\DPD\\Delivery\\DPD', 'OnSaleDeliveryHiddenHTML'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleOrderBeforeSaved',
		'callback' => array('\\Ipolh\\DPD\\EventListener', 'validateDeliveryInfo'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
 	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleComponentOrderOneStepComplete',
		'callback' => array('\\Ipolh\\DPD\\EventListener', 'saveDeliveryInfo'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
	),

	array(
		'module'   => 'sale',
		'name'     => 'OnSaleComponentOrderComplete',
		'callback' => array('\\Ipolh\\DPD\\EventListener', 'saveDeliveryInfo'),
		'sort'     => 100,
		'path'     => '',
		'args'     => array(),
 	),

 	array(
 		'module'   => 'main',
 		'name'     => 'OnEpilog',
 		'callback' => array('\\Ipolh\\DPD\\EventListener', 'showAdminForm'),
 		'sort'     => 100,
 		'path'     => '',
 		'args'     => array(),
 	),
);