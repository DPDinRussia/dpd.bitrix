BX.ready(function() {
	window.ipolOptionsSenderFormHelper = new DpdOptionsSenderFormHelper();
});

function DpdOptionsSenderFormHelper()
{
	if (this === window) {
		return new DpdOptionsSenderFormHelper();
	}

	this.init();
}

DpdOptionsSenderFormHelper.prototype.init = function()
{
	this.initButton();
	this.initDialog();
	this.initEvents();
}

DpdOptionsSenderFormHelper.prototype.initButton = function()
{
    BX.bind(BX('b-ipol-dpd-options-sender-add'), 'click', BX.proxy(function() {
        this.open();
    }, this));
}

DpdOptionsSenderFormHelper.prototype.initDialog = function()
{
	this.DIV = BX('IPOLH_DPD_OPTIONS_SENDER_FORM');

	this.dialog = new BX.CAdminDialog({
        title: BX.message('IPOLH_DPD_OPTIONS_SENDER_DIALOG_TITLE'),
        content: this.DIV,
        icon: 'head-block',
        width: 800,
        height: 500,
        resizable: true,
        draggable: true,
        buttons: [
            {
                id        : 'IPOLH_DPD_ORDER_BUTTON_OPTIONS_SENDER_SAVE',
                className : 'adm-btn-save',
                title     : BX.message('IPOLH_DPD_BUTTON_SAVE'),
                action    : BX.proxy(this.process, this)
            },

            BX.CDialog.prototype.btnClose
        ],
    });
}

DpdOptionsSenderFormHelper.prototype.initEvents = function()
{

}

DpdOptionsSenderFormHelper.prototype.open = function(index)
{
    this.dialog.SetContent('');

    this.process(index, BX.proxy(this.dialog.Show, this.dialog));
}

DpdOptionsSenderFormHelper.prototype.process = function(index, callback)
{
	if (this._ajaxRequest) {
		return;
	}

	var form = this.dialog.GetForm();

    this.showLoading();
    
	this._ajaxRequest = BX.ajax({
		method: 'POST',
		url: form ? form.getAttribute('action') : '/bitrix/admin/ipolh_dpd_options_sender.php?index='+ (index || ''),
		data: this.dialog.GetParameters(),
		dataType: 'html',
		onsuccess: BX.proxy(function(response) {
			try {
				this.dialog.SetContent(response);
				this.DIV = BX('IPOLH_DPD_OPTIONS_SENDER_FORM');

				this.initEvents();
                this.hideLoading();

                callback && callback();
                
			} catch (e) {
				console.log(e);
			}

			this._ajaxRequest = false;
		}, this)
	});
}

DpdOptionsSenderFormHelper.prototype.close = function()
{
	this.dialog.Close();
}


DpdOptionsSenderFormHelper.prototype.showLoading = function(btn)
{
	this.hideLoading();
	
	this._loading = BX.showWait();
	this._loading_btn = btn ? this.dialog.showWait(btn) : false;
}

DpdOptionsSenderFormHelper.prototype.hideLoading = function(btn)
{
	this._loading && BX.closeWait(this._loading);
	this._loading_btn && this.dialog.closeWait(btn);

	this._loading = false;
	this._loading_btn = false;
}